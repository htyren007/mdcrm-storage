﻿using ConsoleStorage.Command;
using ConsoleStorage.INI;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;

namespace ConsoleTest
{
    internal class ExportSolution : AConsoleWriter
    {
        private const string PARAM_NAME = "param";
        private const string ORG_URL = "organizationUrl";
        private const string USER_NAME = "username";
        private const string PASS = "password";
        private const string DOMAIN = "domain";
        private const string SOLUTION = "solutionName";
        private const string PATH = "exportFolderPath";

        public ExportSolution()
        {
        }

        internal void Run()
        {
            var ini = IniData.LoadFile("params.ini");
            // CreateParams(ini);
            if (!ini.Groups.Any(x => x.Name == PARAM_NAME))
            {
                CreateParams(ini);
                return;
            }
            Line("Загружаю параметры!");
            var param = ini.GetGroup(PARAM_NAME);
            var uri = new Uri(param.GetString(ORG_URL));
            var user = param.GetString(USER_NAME);
            var pass = param.GetString(PASS);
            var solution = param.GetString(SOLUTION);
            var path = param.GetString(PATH);
            var domain = param.GetString(DOMAIN);
            Directory.CreateDirectory(path);
            Export2(uri, user, pass, solution, path, domain);
        }

        private void Export2(Uri uri, string user, string pass, string solution, string path, string domain)
        {
            Line("Открываю подключение!");
            var credentials = new ClientCredentials();
            credentials.Windows.ClientCredential = new NetworkCredential(user, pass, domain);
            var proxy = new OrganizationServiceProxy(uri, null, credentials, null);
            proxy.Timeout = TimeSpan.FromMinutes(9);

            Line("Формирую запрос!");
            var request = new ExportSolutionRequest();
            request.SolutionName = solution;
            try
            {
                Line("Выполняю!");
                var response = proxy.Execute(request);

                Line("Создаю файл");
                var archiveName = Path.Combine(path, solution + DateTime.Now.ToString("_dd_MMMM_HH-mm-ss") + ".zip");
                if (response is ExportSolutionResponse solutionResponse)
                    File.WriteAllBytes(archiveName, solutionResponse.ExportSolutionFile);
                Line("Файл создан!");
            }
            catch (Exception ex)
            {
                Error(ex);

            }
        }

        private void Export1(Uri uri, string user, string pass, string solution, string path)
        {
            Line("Создаю объекты!");

            var credentials = new ClientCredentials();
            credentials.UserName.UserName = user;
            credentials.UserName.Password = pass;
            var proxy = new OrganizationServiceProxy(uri, null, credentials, null);
            proxy.Timeout = TimeSpan.FromMinutes(9);

            var request = new ExportSolutionRequest();
            request.SolutionName = solution;

            Line("Выполняю запрос!");
            try
            {
                var response = proxy.Execute(request);

                Line("Создаю файл");
                var archiveName = Path.Combine(path, solution + DateTime.Now.ToString("_dd_MMMM_HH-mm-ss") + ".zip");
                if (response is ExportSolutionResponse solutionResponse)
                    File.WriteAllBytes(archiveName, solutionResponse.ExportSolutionFile);
            }
            catch (Exception ex)
            {
                Error(ex);

            }
        }

        private void CreateParams(IniData ini)
        {
            var param = ini.GetGroup(PARAM_NAME);
            param.SetString(ORG_URL, "http://10.51.66.232/mssd/XRMServices/2011/Organization.svc", "IP-adress CRM");
            param.SetString(USER_NAME, "precustd-admin", "Логин пользователя");
            param.SetString(PASS, "ou42qkNWK0ks7ZB", "Пароль");
            param.SetString(SOLUTION, "YOTASD_Base", "Решение");
            param.SetString(PATH, @"d:\GMCS\SolutionExport", "Путь");
            param.SetString(DOMAIN, @"scarteldc", "Домен");
            ini.SaveFile();
        }
    }
}