﻿using ConsoleStorage;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Timers;

namespace ConnectionKeeper4U
{
    internal class Keeper : AConsoleWriter
    {
        private static Keeper instance;
        private Timer timer;
        private int Index = 0;
        private int current;

        public static Keeper Instance
        {
            get
            {
                if (instance == null)
                    instance = new Keeper();
                return instance;
            }
        }

        public KeeperConnectionStatus Status { get; private set; } = KeeperConnectionStatus.Unknown;
        public bool IsRun =>  (timer?.Enabled) ?? false;

        internal void Get(string uri, bool messageAfterRequest)
        {
            HttpWebRequest http = WebRequest.CreateHttp(uri);
            http.Method = HttpMethod.Get.Method;
            DefaultHeaders(http);
            try
            {
                //http.Timeout = 100000;
                using (HttpWebResponse response = (HttpWebResponse)(http.GetResponse()))
                {
                    if (Settings.Instance.General.IsSaveResponses)
                    {
                        using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
                        {
                            File.AppendAllText(Settings.Instance.General.ResponseFile, $"\n===== {DateTime.Now} =====\n" + rdr.ReadToEnd());
                        }
                    }
                    if (messageAfterRequest)
                        Text($"Успешный запрос {++Index}!");
                    Status = KeeperConnectionStatus.Connected;
                }
            }
            catch (Exception ex)
            {
                if (messageAfterRequest)
                {
                    Line();
                    Line($"Ошибка запроса {++Index}. ");
                    Error(ex);
                }
                if (Settings.Instance.General.StopAfterError)
                    Stop();
                File.AppendAllText(Settings.Instance.General.ResponseFile, 
                    $"\n===== Ошибка запроса {Index}: {DateTime.Now} =====\n{ex.GetType().Name}: {ex.Message}\nuri: {uri}\n");
                if (ex is WebException webEx)
                {
                    if (webEx.Status == WebExceptionStatus.ConnectFailure)
                        Status = KeeperConnectionStatus.Diconneced;
                }
            }
        }

        internal void Stop()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
            }
        }

        internal void Start()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
            }
            timer = new Timer(Settings.Instance.General.Interval * 60 * 1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();
            // Line($"Хранитель запущен с интервалом {Settings.Instance.General.Interval} минут");
            // Line("");
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            Text($"\r");
            timer.Stop();
            NextRequest();
            timer.Start();
        }

        internal void NextRequest()
        {
            int count = Settings.Instance.Links.Count;
            current = (count + current + 1) % count;
            Get(Settings.Instance.Links[current], Settings.Instance.General.MessageAfterRequest);
        }

        private void DefaultHeaders(HttpWebRequest http)
        {
            http.UserAgent = ".Net ConnectionKeeper";
            http.Accept = "*/*";
        }
    }

}
