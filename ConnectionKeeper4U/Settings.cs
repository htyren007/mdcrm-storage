﻿using ConsoleStorage.INI;
using System;
using System.Diagnostics;
using System.Linq;

namespace ConnectionKeeper4U
{
    public class Settings : SettingsBase
    {
        private const string SETTING_FILENAME = "data.ini";
        private static Settings instance;

        public Settings(string filename) : base(filename)
        {
            General = new GeneralSettingsGroup(file);
            Links = new LinksSettingsGroup(file);
            Credentials = new CitrixCredentialsGroup(file);
            
        }

        public static Settings Instance
        {
            get
            {
                if (instance == null)
                    Load();
                return instance;
            }
        }


        public GeneralSettingsGroup General { get; private set; }
        public LinksSettingsGroup Links { get; private set; }
        public CitrixCredentialsGroup Credentials { get; private set; }

        public static void Load(bool forceSave = false)
        {
            instance = new Settings(SETTING_FILENAME);
            if (forceSave)
            {
                instance.General.Force();
                instance.Links.Force();
                Save();
            }
        }

        public static void Save() => instance.SaveSettings();

        internal static void Open()
        {
            Process.Start("notepad.exe", SETTING_FILENAME);
        }

        public class GeneralSettingsGroup : SettingsGroup
        {
            private const string GROUPNAME = "general";

            public GeneralSettingsGroup(IniData file) : base(file, GROUPNAME)
            {
            }
            public double Interval { get => GetDoubleSetting(); set => SetDoubleSetting(value); }
            public string ResponseFile { get => GetStringSetting(); set => SetStringSetting(value); }
            public bool IsSaveResponses { get => GetBoolSetting(); set => SetBoolSetting(value); }
            public string Processes { get => GetStringSetting(); set => SetStringSetting(value); }
            /// <summary>Автозапуск хранителя при запуске приложения</summary>
            public bool AutoStart { get => GetBoolSetting(); set => SetBoolSetting(value); }
            /// <summary>Останов хранителя при ошибке</summary>
            public bool StopAfterError { get => GetBoolSetting(); set => SetBoolSetting(value); }
            public bool MessageAfterRequest { get => GetBoolSetting(); set => SetBoolSetting(value); }

            protected override void CreateDefault()
            {
                base.CreateDefault();
                group.SetBool(nameof(AutoStart), true, "Автозапуск хранителя при запуске приложения");
                group.SetBool(nameof(StopAfterError), false, "Останов хранителя при ошибке");
                group.SetBool(nameof(MessageAfterRequest), false, "Сообщение после запроса!");
                group.SetBool(nameof(IsSaveResponses), false, "Необходимо ли логировать ответы");

                group.SetReal(nameof(Interval), 3, "Период отправки запроса", "В минутах, возможно десятичное значение");
                group.SetString(nameof(ResponseFile), "Response.txt", "Файл для записи ответов");
                group.SetString(nameof(Processes), "nsload,nsverctl", "Процессы для ужичтожения");
            }

            public void Force() => CreateDefault();
        }
        public class CitrixCredentialsGroup : SettingsGroup
        {
            private const string GROUPNAME = "credentials";

            public CitrixCredentialsGroup(IniData file) : base(file, GROUPNAME)
            {
            }
            public string Login { get => CodeCredentials(GetStringSetting()); set => SetStringSetting(CodeCredentials(value)); }
            public string Pass { get => CodeCredentials(GetStringSetting()); set => SetStringSetting(CodeCredentials(value)); }

            protected override void CreateDefault()
            {
                base.CreateDefault();

                group.SetString(nameof(Login), CodeCredentials("yotaru\\rmiroshnikov") );
                group.SetString(nameof(Pass), CodeCredentials("pGDDuYhE!1Vj"));
            }

            private string CodeCredentials(string text)
            {
                return ConsoleStorage.Data.Coder.CodeB(text, "Renat");
            }


        }
        public class LinksSettingsGroup : SettingsGroup
        {
            private const string GROUPNAME = "links";

            public LinksSettingsGroup(IniData file) : base(file, GROUPNAME)
            {
            }

            public string this[int index] => group.ElementAt(index).Value;
            public string Key(int index) => group.ElementAt(index).Name;
            public int Count => group.Count();

            protected override void CreateDefault()
            {
                base.CreateDefault();
                group.Comment = new System.Collections.Generic.List<string>() { "Пути к которым по очереди происходит запрос" };
                group.SetString("identification1", "http://ft2-cd-api.g4lab.com:8080/cdm/v1/customer/14c3f351754049f09f2172d6382f714b/identification");
                group.SetString("identification2", "http://ft3-cd-api.g4lab.com:8080/cdm/v1/customer/14c3f351754049f09f2172d6382f714b/identification");
                group.SetString("identification3", "http://ft2-cd-api.g4lab.com:8080/cdm/v1/customer/62742b3628774ff086335ca6fa4d43e9/identification");
                group.SetString("identification4", "http://ft3-cd-api.g4lab.com:8080/cdm/v1/customer/62742b3628774ff086335ca6fa4d43e9/identification");
                group.SetString("identification5", "http://ft2-cd-api.g4lab.com:8080/cdm/v1/customer/21db4216cdab40c1b00cc1fbf5bd2bd1/identification");
                group.SetString("identification6", "http://ft3-cd-api.g4lab.com:8080/cdm/v1/customer/21db4216cdab40c1b00cc1fbf5bd2bd1/identification");

            }

            public void Force() => CreateDefault();
        }
    }

}
