﻿using ConsoleStorage.Command;
using ConsoleStorage.Utility;
using System;
using System.Diagnostics;
using System.Linq;

namespace ConnectionKeeper4U.OldUI
{
    internal class MainCommands : SubCommandCollection
    {
        private MainController controller;
        public MainCommands()
        {
            Settings.Load();
            controller = new MainController();
            controller.ConsoleMessage += WriteMessage;

            AddSimple("quit", () => IsWork = false, "Выход");
            AddSimple("clear", Clear, "Очистить экран");

            AddSubCollection("keeper",
                new IntegerCommand("req", controller.GetRequest) { DisplayName = "Отправить запрос" },
                new SimpleCommand("restart", controller.RestartKeeper) { DisplayName = "Перезапустить хранитель" },
                new SimpleCommand("stop", Keeper.Instance.Stop) { DisplayName = "Остановить хранитель" },
                new SimpleCommand("start", Keeper.Instance.Start) { DisplayName = "Запустить хранитель" });

            var keeper = GetCollection("keeper");
            keeper.Add(new ListCommand(keeper));
            keeper.Add(new HelpCommand(keeper) { WriteCommand = WriteHelp });
            
            AddSubCollection("info",
                new SimpleCommand("interval", controller.GetInterval) { DisplayName = "Отобразить интервал" },
                new SimpleCommand("log", controller.GetIsSaveResponses) { DisplayName = "Отобразить интервал" },
                new SimpleCommand("login", controller.GetLogin) { DisplayName = "Установить данные" },
                new SimpleCommand("pass", controller.GetPass) { DisplayName = "Установить данные" });
            var info = GetCollection("info");
            info.Add(new ListCommand(info));
            info.Add(new HelpCommand(info) { WriteCommand = WriteHelp });

            AddSubCollection("settings",
                new SimpleCommand("open", Settings.Open) { DisplayName = "Открыть настройки "},
                new StringCommand("log", controller.SetLog) { DisplayName = "Управление логированием", Help = "log on - включает логирование, log off - выключает" },
                new IntegerCommand("interval", controller.SetInterval) { DisplayName = "Установить данные" },
                new SimpleCommand("login", controller.SetLogin) { DisplayName = "Установить данные" },
                new SimpleCommand("pass", controller.SetPass) { DisplayName = "Установить данные" },
                new BoolCommand("auto", controller.SetAuto) { DisplayName = "Установить данные" });

            var settings = GetCollection("settings");
            settings.Add(new ListCommand(settings));
            settings.Add(new HelpCommand(settings) { WriteCommand = WriteHelp });

            AddSimple("open-log", controller.OpenLog, "Открыть лог " );

            AddSubCollection("citrix",
                new SimpleCommand("kill", controller.CitrixKill) { DisplayName = "Завершить процесс цитрикс" },
                new SimpleCommand("list", controller.CitrixList) { DisplayName = "Вывести процессы которые будут остановлены командой citrix/kill" },
                new SimpleCommand("run", controller.CitrixRun) { DisplayName = "Запустить citrix" },
                new SimpleCommand("login", controller.GetLogin),
                new SimpleCommand("pass", controller.GetPass));

            var citrix = GetCollection("citrix");
            citrix.Add(new ListCommand(citrix));
            citrix.Add(new HelpCommand(citrix) { WriteCommand = WriteHelp });

            commands.Add(new HelpCommand(this) { WriteCommand = WriteHelp });
            commands.Add(new ListCommand(this));
            AddSimple("status", controller.Status, "Текуший статус");   
        }

        private void WriteMessage(string text)
        {
            Line(text);
        }

        private void WriteHelp(AConsoleCommand command, bool inDetail)
        {
            Line($"{command.CommandName,-8} {command.DisplayName}");
        }
    }
}
