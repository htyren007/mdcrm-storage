﻿using ConsoleStorage.Slides;
using System;

namespace ConnectionKeeper4U.OldUI
{
    internal class MainMenu : MenuSlide
    {
        public MainMenu()
        {
            Initialize();
        }

        private void Initialize()
        {
            Header.Text = "ConnectionKeeper4U";

            AddItem(new MenuItem() { Title = "Отправить" });
            AddItem(new MenuItem() { Title = "Настроить" });
            AddItem(new MenuItem() { Title = "Интерфейс 1.0" });
            AddItem(new MenuItem() { Title = "Выход" , Action = ()=>NextSlide = null });
        }
    }
}