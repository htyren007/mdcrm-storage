﻿using ConsoleStorage;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Utility;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace ConnectionKeeper4U.OldUI
{
    internal class MainController: AConsoleWriter
    {
        internal Action<string> ConsoleMessage;

        public MainController()
        {
            if (Settings.Instance.General.AutoStart)
                Keeper.Instance.Start();
        }

        internal void GetRequest(int obj)
        {
            if (Settings.Instance.Links.Count <= obj)
            {
                Line("Индекс вышел за пределы массива");
                return;
            }

            Line($"{Settings.Instance.Links.Key(obj)}: {Settings.Instance.Links[obj]}");
            Keeper.Instance.Get(Settings.Instance.Links[obj], Settings.Instance.General.MessageAfterRequest);
        }

        internal void Status()
        {
            Keeper.Instance.NextRequest();
            Line("Статус соединения: ", Keeper.Instance.Status.ToString());
        }

        internal void RestartKeeper()
        {
            Keeper.Instance.Stop();
            Line("Хранитель остановлен!");
            Keeper.Instance.Start();
        }

        internal void GetInterval() => Line(Settings.Instance.General.Interval.ToString(), "минут");

        internal void GetIsSaveResponses() => Line(Settings.Instance.General.IsSaveResponses.ToString());

        internal void GetLogin() => Line(Settings.Instance.Credentials.Login);

        internal void GetPass() => Line(Settings.Instance.Credentials.Pass);

        internal void SetInterval(int value)
        {
            Settings.Instance.General.Interval = value;
            Settings.Save();
        }

        internal void SetLogin()
        {
            Settings.Instance.Credentials.Login = ConsoleHelper.Query("\nВведите login:");
            Settings.Save();
        }

        internal void SetPass()
        {
            Settings.Instance.Credentials.Pass = ConsoleHelper.Query("\nВведите password:");
            Settings.Save();
        }

        internal void SetAuto(bool obj)
        {
            Settings.Instance.General.AutoStart = obj;
            Settings.Save();
        }

        internal void SetLog(string obj)
        {
            obj = obj.ToLower();
            if (obj == "on")
                Settings.Instance.General.IsSaveResponses = true;
            if (obj == "off")
                Settings.Instance.General.IsSaveResponses = false;
            Settings.Save();
        }

        internal void OpenLog() => Process.Start("notepad.exe", Settings.Instance.General.ResponseFile);

        internal void CitrixKill()
        {
            try
            {
                var procNames = Settings.Instance.General.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var processes = Process.GetProcesses();
                foreach (Process proc in processes)
                {
                    if (procNames.Any(x => proc.ProcessName.Contains(x)))
                    {
                        proc.Kill();
                        Line($"Процесс уничтожен: {proc.ProcessName}({proc.Id})");
                    }
                }
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        internal void CitrixList()
        {
            try
            {
                var procNames = Settings.Instance.General.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                Line("Процессы");
                var processes = Process.GetProcesses();
                foreach (Process proc in processes)
                {
                    try
                    {
                        if (procNames.Any(x => proc.ProcessName.Contains(x)))
                        {
                            Line(proc.Id.ToString(), proc.ProcessName, proc.MainModule.ModuleName);
                        }
                    }
                    catch
                    {
                        Error($"Не возможно получить заппые процесса {proc.ProcessName}!");
                    }
                }
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        internal void CitrixRun()
        {
            var citrixName = @"C:\Program Files\Citrix\Secure Access Client\nsload.exe";
            var login = Settings.Instance.Credentials.Login;
            var pass = Settings.Instance.Credentials.Pass;
            try
            {
                var process = Process.Start(citrixName);

                Line("Приложение запускается");
                Line(login);
                Line(pass);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }
    }
}