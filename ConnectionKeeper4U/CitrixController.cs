﻿using ConsoleStorage;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace ConnectionKeeper4U
{
    [SlideTitle("ConnectionKeeper4U", left: 30, top: 0)]
    internal class CitrixController : AConsoleWriter
    {
        private CustomForm slide;
        private TextField headerProcesses;
        private List<TextField> fieldProcesses;

        public CitrixController()
        {
            Keeper.Instance.NextRequest();
            if (Keeper.Instance.Status == KeeperConnectionStatus.Connected)
            {
                Message = "Соединение есть";
            }
        }

        [SlideLabel(left: 30, top: 0)]
        public string HeaderText { get; set; } = "=== ConnectionKeeper4U ===";

        


        [SlideLabel(left: 0, top: 27)]
        public string Message { get; set; } = "";


        [SlideButton(dispayName: "<= Назад", left:1, top:0)]
        public void Back(CustomForm slide)
        {
            slide.NextSlide = null;
        }


        [SlideButton(hint: "Запуск Citrix", left:5, top:1)]
        public void CitrixRun()
        {
            var citrixName = @"C:\Program Files\Citrix\Secure Access Client\nsload.exe";
            var procNames = Settings.Instance.General.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var processes = ProcessHelper.FindProcessesByName(procNames);
            if (processes.Count() > 0)
            {
                if (Keeper.Instance.Status == KeeperConnectionStatus.Connected)
                {
                    Message = "Соединение есть";
                    return;
                }
            }
            try
            {
                var process = Process.Start(citrixName);

                Message = "Приложение запускается!";
                //Line(login);
                //Line(pass);
            }
            catch (Exception ex)
            {
                Message = "";
                Error(ex);
                Console.ReadLine();
            }
        }

        [SlideButton(dispayName: "Login", left: 20, top: 1)]
        public void Login()
        {
            var login = Settings.Instance.Credentials.Login;
            Clipboard.SetText(login);
            Message = $"Скопировано '{login}'";
        }

        [SlideButton(dispayName: "Pass", left: 35, top: 1)]
        public void Pass()
        {
            var pass = Settings.Instance.Credentials.Pass;
            Clipboard.SetText(pass);
            Message = $"Скопировано '{pass}'";
        }

        [SlideButton(hint: "Уничтожить процессы Citrix", left: 50, top: 1)]
        public void CitrixKill()
        {
            try
            {
                var procNames = Settings.Instance.General.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var processes = ProcessHelper.FindProcessesByName(procNames);
                int count = 0;
                foreach (Process proc in processes)
                {
                    proc.Kill();
                    count++;
                }
                Message = $"Уничтожено {count} процессов!";
            }
            catch (Exception ex)
            {
                Error(ex);
                Console.ReadLine();
            }
        }

        [SlideSetup]
        public void Setup(CustomForm form)
        {
            slide = form;
            fieldProcesses = new List<TextField>();
            headerProcesses = new TextField() { Left = 5, Top = 3 };
            UpdateProcesses();
        }

        private void UpdateProcesses()
        {
            var top = headerProcesses.Top + 1;
            foreach (var item in fieldProcesses)
            {
                slide.RemoveElement(item);
            }
            var procNames = Settings.Instance.General.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            slide.Add(headerProcesses);
            int count = 0;
            foreach (var item in ProcessHelper.FindProcessesByName(procNames))
            {
                var field = new TextField() { Content = $"{item.Id} {item.ProcessName}({item.MachineName})", Left = 6, Top = top++ };
                fieldProcesses.Add(field);
                slide.Add(field);
                count++;
            }
            headerProcesses.Content = $"Процессы ({count})";
        }
    }

    internal class ProcessHelper
    {
        internal static IEnumerable<Process> FindProcessesByName(string[] procNames)
        {
            var processes = Process.GetProcesses();
            foreach (Process proc in processes)
            {
                if (procNames.Any(x => proc.ProcessName.Contains(x)))
                {
                    yield return proc;
                }
            }
        }
    }
}