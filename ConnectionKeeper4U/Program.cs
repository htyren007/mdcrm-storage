﻿using ConsoleStorage.Command;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using System;
using System.Threading.Tasks;

namespace ConnectionKeeper4U
{
    internal class Program
    {
        internal static Executor exe;

        public static Screen screen { get; private set; }

        [STAThread]
        public static void Main(string[] args)
        {
            var menu = new Options<MainController>();
            menu.Run();
        }

        internal static void Run1_0()
        {
            exe = new Executor();
            exe.SetCommands(new OldUI.MainCommands());
            exe.Run();
        }
    }
}
