﻿using ConsoleStorage;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.Utility;
using System;
using System.Diagnostics;
using System.Linq;

namespace ConnectionKeeper4U
{
    [SlideTitle("ConnectionKeeper4U")]
    internal class MainController : AConsoleWriter
    {
        public MainController()
        {
            Settings.Load();

        }
        [SlideLabel(left:40, top:3)]
        public string StatusConnect { get => "Нет контакта"; }

        [SlideButton(hint: "Запустить хранитель!")]
        public void Start()
        {
            if (!Keeper.Instance.IsRun)
            {
                Keeper.Instance.Start();
                Line($"Хранитель запущен с интервалом {Settings.Instance.General.Interval} минут");
                ConsoleHelper.Query("");
            }
        }

        [SlideButton(hint: "Сделать запрос")]
        public void GetRequest()
        {
            while (true)
            {
                var text = ConsoleHelper.Query($"Введите число от 0 до {Settings.Instance.Links.Count - 1}: ");
                if (int.TryParse(text, out int index))
                {
                    if (Settings.Instance.Links.Count <= index)
                    {
                        Line("Индекс вышел за пределы массива");
                        return;
                    }
                    Line($"{Settings.Instance.Links.Key(index)}: {Settings.Instance.Links[index]}");
                    Keeper.Instance.Get(Settings.Instance.Links[index], true);
                }
                else if (text.ToLower() == "all")
                {

                    for (int i = 0; i < Settings.Instance.Links.Count; i++)
                    {
                        Line($"{Settings.Instance.Links.Key(i)}: {Settings.Instance.Links[i]}");
                        Keeper.Instance.Get(Settings.Instance.Links[i], true);
                        Line("");
                    }
                }
                else break;
            }
        }

        

        [SlideButton(hint: "Управление процессами Citrix")]
        public void CitrixRun()
        {
            var menu = new Options<CitrixController>();
            menu.Run();
        }

        [SlideButton(hint:"Вывести статус")]
        public void Status()
        {
            Keeper.Instance.NextRequest();
            Line("Статус соединения: ", Keeper.Instance.Status.ToString());
            ConsoleHelper.Query("");
        }

        [SlideButton(dispayName:"Настройки", hint:"Войти в меню настройки")]
        public void SetSetting()
        {
            var menu = new Options<SettingController>();
            menu.Run();

            Settings.Save();
        }

        [SlideButton(dispayName: "Выход")]
        public void Back(ASlide slide)
        {
            slide.NextSlide = null;
        }
    }
}