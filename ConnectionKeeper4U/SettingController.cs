﻿using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;

namespace ConnectionKeeper4U
{
    [SlideTitle("ConnectionKeeper4U")]
    public class SettingController
    {
        [SlideProperty(dispayName: "Автозапуск хранителя")]
        public bool AutoStart
        {
            get => Settings.Instance.General.AutoStart;
            set => Settings.Instance.General.AutoStart = value;
        }

        [SlideProperty(dispayName: "Остановить при ошибке")]
        public bool StopAfterError
        {
            get => Settings.Instance.General.StopAfterError;
            set => Settings.Instance.General.StopAfterError = value;
        }

        [SlideProperty()]
        public string Interval
        {
            get => Settings.Instance.General.Interval.ToString();
            set
            {
                if (double.TryParse(value, out double interval))
                    Settings.Instance.General.Interval = interval;
            }
        }

        [SlideButton(dispayName:"Назад")]
        public void Back(ASlide slide)
        {
            slide.NextSlide = null;
        }
    }
}