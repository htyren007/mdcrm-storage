﻿using hty.TextTools.CodeGenerator;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeGenerator
{
    public class EntityClassGenerator
    {
        EntityMetadata meta;
        List<string> attributies = new List<string>();

        public List<string> Attributies { get => attributies; set => attributies = value; }
        public string NameSpase{ get; set; }

        public EntityClassGenerator(EntityMetadata meta) => this.meta = meta;



        public string Create()
        {
            CodeFile file = new CodeFile();

            var ns = file.AddNameSpase(NameSpase);

            file.AddUsing("Microsoft.Xrm.Sdk");
            file.AddUsing("Microsoft.Xrm.Sdk.Client");
            file.AddUsing("System.ComponentModel");
            file.AddUsing("System.Runtime.Serialization");
            //file.AddUsing("");
            //file.AddUsing("");

            string schemaName = meta.SchemaName;
            string className = GetNameElement(schemaName);
            var entityClass = ns.AddClass(className);
            ns.AddAttribute("assembly: ProxyTypesAssembly()");

            entityClass.AddAttribute("DataContract");
            entityClass.AddAttribute($"EntityLogicalName(\"{schemaName}\")");

            entityClass.AddBase("Entity");
            entityClass.AddBase("INotifyPropertyChanging");
            entityClass.AddBase("INotifyPropertyChanged");

            entityClass.AddText($@"public const string EntityLogicalName = ""{schemaName}"";");
            if (meta.ObjectTypeCode.HasValue)
                entityClass.AddText($@"public const int EntityTypeCode = {meta.ObjectTypeCode.Value};");

            entityClass.AddText($"public {className}() : base(EntityLogicalName) {{}};");

            foreach (var attribut in Attributies)
            {
                var atrMeta = meta.Attributes.FirstOrDefault(atr => atr.LogicalName == attribut);
                if (atrMeta != null)
                {
                    string propName = GetNameElement(atrMeta.LogicalName);
                    string type = GetTypeString(atrMeta.AttributeType);
                    
                    var prop = entityClass.AddProperty(type, propName);

                    prop.AddAttribute($"AttributeLogicalName(\"{atrMeta.LogicalName}\")");

                    prop.GetCode = $@"this.GetAttributeValue<{type}>(""{atrMeta.LogicalName}"");";
                    prop.SetCode = $"OnPropertyChanging();\n SetAttributeValue(\"{atrMeta.LogicalName}\", value);\nOnPropertyChanged();";
                }
            }
            string fileName = $"{meta.SchemaName}.cs";
            file.Save(fileName);
            return fileName;
        }

        private static string GetTypeString(AttributeTypeCode? attributeType)
        {
            switch (attributeType)
            {
                case AttributeTypeCode.Uniqueidentifier: return "Guid";
                case AttributeTypeCode.Boolean: return "bool";
                case AttributeTypeCode.DateTime: return "DateTime";
                case AttributeTypeCode.Decimal: return "decimal";
                case AttributeTypeCode.Integer: return "int";
                case AttributeTypeCode.Double: return "double";
                case AttributeTypeCode.String: return "string";
                case AttributeTypeCode.Memo: return "string";

                default:
                    throw new NotImplementedException($"Метод или операция не реализована {attributeType}");
            }
        }

        private static string GetNameElement(string name)
        {
            return name.Substring(0, 1).ToUpper() + name.Substring(1, name.Length - 1);
        }
    }
}