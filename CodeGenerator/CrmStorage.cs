﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System;
using Microsoft.Xrm.Sdk.Metadata;
using System.Linq;
using System.Collections.Generic;

namespace CodeGenerator
{
    public class CrmStorage
    {
        public static EntityMetadata[] GetMetadataAllEntities(IOrganizationService service)
        {
            RetrieveAllEntitiesRequest request = new RetrieveAllEntitiesRequest();
            request.EntityFilters = EntityFilters.Entity;

            // Execute the request.
            RetrieveAllEntitiesResponse response = (RetrieveAllEntitiesResponse)service.Execute(request);
            EntityMetadata[] entities = response.EntityMetadata;
            return entities;
        }

        public static EntityMetadata GetMetadata(IOrganizationService service, string logicalName)
        {
            RetrieveEntityRequest request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.All,
                LogicalName = logicalName,
                RetrieveAsIfPublished = true
            };
            RetrieveEntityResponse response = (RetrieveEntityResponse)service.Execute(request);
            return response.EntityMetadata;
        }

        public static EntityMetadata GetAttributesMetadata(IOrganizationService service, string logicalName)
        {
            RetrieveEntityRequest request = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.Attributes,
                LogicalName = logicalName,
                RetrieveAsIfPublished = true
            };
            RetrieveEntityResponse response = (RetrieveEntityResponse)service.Execute(request);
            return response.EntityMetadata;
        }
    }
}