﻿using ConsoleStorage.Slides;
using System;
using System.Collections.Generic;

namespace CodeGenerator
{
    internal class PremierSlide : ASlide
    {
        private List<ILabel> labels;

        public PremierSlide(string[] args)
        {
            if (args.Length > 0)
            {
                throw new NotImplementedException();
            }

            labels = new List<ILabel>();
            labels.Add(new ColorLabel() { 
                Left = 10, 
                Top = 5, 
                Text = "Генератор Классов Entity", 
                Foreground = ConsoleColor.Blue, 
                Background = ConsoleColor.White 
            });
        }

        public override void BeforeRendering()
        {
            base.BeforeRendering();

            
        }

        public override IEnumerable<ILabel> Labels { get => labels; }

        public override void OnPressKey(ConsoleKeyInfo info)
        {
            if (info.Key == ConsoleKey.Spacebar)
                NextSlide = new ConnectionSlide();

            if (info.Key == ConsoleKey.Escape)
                NextSlide = null;
        }
    }
}