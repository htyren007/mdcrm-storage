﻿using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;

namespace CodeGenerator
{
    internal class ConfigureConnectionSlide : Form
    {
        private ProgramController controller;
        private InputField userName;
        private InputField pass;
        private InputField domain;

        public ConfigureConnectionSlide(): base()
        {
            controller = ProgramController.Instance;

            // Добавить записи
            // Отобразить на экране

            AddLabel(new Label("Имя пользователя: ", 15, 3));
            userName = new InputField() { Left = 15, Top = 4, Width = 15 };
            userName.ValueChange += (s) => controller.Settings.Connection.UserName = s;
            userName.Label = controller.Settings.Connection.UserName;
            AddElement(userName);

            AddLabel(new Label("Пароль: ", 15, 5));
            pass = new InputField() { Left = 15, Top = 6, Width = 15 };
            pass.ValueChange += (s) => controller.Settings.Connection.Pass = s;
            pass.Label = controller.Settings.Connection.Pass;
            AddElement(pass);

            AddLabel(new Label("Домен:  ", 15, 7));
            domain = new InputField() { Left = 15, Top = 8, Width = 15 };
            domain.ValueChange += (s) => controller.Settings.Connection.Domain = s;
            domain.Label = controller.Settings.Connection.Domain;
            AddElement(domain);

            AddElement(new Button() { Label = " Подключится ", Left = 20, Top = 15, Action = Connect });
            AddElement(new Button() { Label = "    Назад    ", Left = 35, Top = 15, Action = Exit });
        }

        private void Connect()
        {
            controller.Connection.Connect();
            NextSlide = new EntitiesSlide();
        }

        private void Exit()
        {
            NextSlide = new ConnectionSlide();
        }
    }
}