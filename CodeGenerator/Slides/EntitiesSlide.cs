﻿using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeGenerator
{
    public class EntitiesSlide : Form
    {
        private ProgramController controller;
        private IEnumerable<Microsoft.Xrm.Sdk.Metadata.EntityMetadata> entities;
        private List<CheckBox> entityBoxes;

        public EntitiesSlide()
        {
            controller = ProgramController.Instance;

            entities = controller.GetEntities();
            int top = 3;
            AddElement(new Button() { Label = "Назад", Left = 6, Top = top, Action = ()=> NextSlide = new ConnectionSlide() });
            AddElement(new Button() { Label = "Генерировать", Left = 26, Top = top, Action=Generate });

            top++;
            AddLabel(new Label("Найденные сушности", 7, top++));
            top++;

            entityBoxes = new List<CheckBox>();
            foreach (var item in entities)
            {
                var box = new CheckBox() { Label = item.LogicalName, Left = 7, Top = top++ };
                entityBoxes.Add(box);
                AddElement(box);
            }
        }

        private void Generate()
        {
            foreach (var item in entityBoxes)
            {
                if (item.IsChecked)
                {
                    var entity = entities.First(q => q.LogicalName == item.Label);
                    controller.AddToExecute(entity);
                }
            }

            NextSlide = new EntityConfigureSlide();
        }
    }
}