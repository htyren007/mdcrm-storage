﻿using ConsoleStorage.Slides;
using System;

namespace CodeGenerator
{
    internal class ConnectionSlide : Menu
    {
        private ProgramController controller;
        private ColorLabel message;

        public ConnectionSlide():base()
        {
            controller = ProgramController.Instance;

            Header.Left = 8;
            Header.Top = 5;
            Header.Text = "Подключение:";

            AddItem(new MenuItem(){
                Title = "1) Настроить подключение",
                Action = ConfigureConnection,
            });

            AddItem(new MenuItem()
            {
                Title = "2) Подключится",
                Action = Connect,
            });

            AddItem(new MenuItem()
            {
                Title = "3) Закрыть программу",
                Action = Exit,
            });

            message = new ColorLabel { Text = "" };
            AddLabel(message);
        }

        private void Exit()
        {
            NextSlide = null;
        }

        private void Connect()
        {
            controller.Connection.Connect();

            // if (res)
                NextSlide = new EntitiesSlide();
            //else
            //    message.Text = "Подключится не удалось!";
        }

        private void ConfigureConnection()
        {
            NextSlide = new ConfigureConnectionSlide();
        }

        public override void OnPressKey(ConsoleKeyInfo info)
        {
            if (info.Key == ConsoleKey.NumPad1)
                ConfigureConnection();
            if (info.Key == ConsoleKey.NumPad2)
                Connect();
            if (info.Key == ConsoleKey.NumPad3 
                || info.Key == ConsoleKey.Execute)
                Exit();

            base.OnPressKey(info);
        }
    }
}