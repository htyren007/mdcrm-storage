﻿using CodeGenerator.Settings;
using ConsoleStorage.Command;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator
{
    internal class ProgramController : AConsoleWriter
    {
        private static object[] SolutionUniqueName = new[] { "WebResources" };

        private static ProgramController instance;
        private MySettings settings;
        private Queue<EntityMetadata> executingEntities = new Queue<EntityMetadata>();

        public ProgramController()
        {
            Settings = MySettings.GetSettings();
        }

        public static ProgramController Instance
        {
            get
            {
                if (instance == null)
                    instance = new ProgramController();
                return instance;
            }
        }

        public MySettings Settings { get => settings; set => settings = value; }
        public MyConnection Connection => settings.Connection;
        public Order Order => settings.Order;

        internal void Run()
        {
            try
            {
                if (Order.GetList)
                    ListHandler();

                if (Order.GetMeta)
                    GetMetaHandler();

                if (Order.GenerateCSharpEntityClasses)
                    GenerateCSharpEntityClassesHandler();
            }
            catch (InvalidOperationException)
            {
                Error("Подключение не установлено!");
                Console.ReadKey();
            }


            Settings.Save();
        }

        private void GenerateCSharpEntityClassesHandler()
        {
            const string dir = "Models";
            Directory.CreateDirectory(dir);
            Connection.Connect();
            var service = Connection.OrganizationService;
            var baseFilename = Path.Combine(dir, "AEntity.cs");
            if (!File.Exists(baseFilename))
            {
                Line("Создаю базовый файл");
                File.WriteAllText(baseFilename, CrmGeneratorClass.BaseClass(Order.CSharpOrder.NameSpace));
            }

            foreach (var orderClass in Order.CSharpOrder.Classes)
            {
                Line($"Создаю запрос получения метаданных сущности {orderClass.LogicalName}") ;
                EntityMetadata meta = CrmStorage.GetMetadata(service, orderClass.LogicalName);
                Line($"Создаю класс {meta.LogicalName}");
                string codeClass = CrmGeneratorClass.Generate(meta, Order.CSharpOrder);
                var filename = Path.Combine(dir, meta.SchemaName + ".cs");
                Line($"Записываю в файл '{filename}'");
                if (File.Exists(filename)) 
                    File.Delete(filename);
                File.WriteAllText(filename, codeClass);
            }

            Order.GenerateCSharpEntityClasses = false;
        }

        private void GetMetaHandler()
        {
            const string dir = "Entities";
            Line("Создаю запрос получения метаданных");
            Directory.CreateDirectory(dir);
            Connection.Connect();
            var service = Connection.OrganizationService;

            Line("Жду ответа!");
            var entities = CrmStorage.GetMetadataAllEntities(service);
            Line("Записываю в файлs!");
            foreach (var logicalName in Order.Meta_Names)
            {
                var entity = entities.FirstOrDefault(en => en.LogicalName == logicalName);
                if (entity != null)
                {
                    Line(entity.LogicalName);
                    Line("Жду ответа!");
                    EntityMetadata meta  = CrmStorage.GetAttributesMetadata(service, entity.LogicalName);
                    Line("Записываю в файл!");
                    var filename = Path.Combine(dir, meta.LogicalName + ".txt");
                    using (var writer = new StreamWriter(File.OpenWrite(filename)))
                    {
                        foreach (var attr in meta.Attributes.OrderBy(att=>att.LogicalName))
                        {
                            writer.Write(attr.LogicalName);
                            writer.Write("\t");
                            writer.Write(attr.SchemaName);
                            writer.Write("\t");
                            writer.WriteLine(attr.AttributeType);

                        }

                    }
                }
            }

            Order.GetMeta = false;
        }

        private void ListHandler()
        {
            Line("Создаю запрос получения решения");
            Connection.Connect();
            var service = Connection.OrganizationService;

            Line("Жду ответа!");
            var entities = CrmStorage.GetMetadataAllEntities(service);
            Line("Записываю в файл!");
            using (var writer = new StreamWriter(File.OpenWrite(Order.List_OutputFile)))
            {
                foreach (var entity in entities.OrderBy(ent=>ent.LogicalName))
                {
                    Line(entity.LogicalName);
                    writer.Write(entity.LogicalName);
                    writer.Write("\t");
                    writer.WriteLine(entity.SchemaName);
                }
            }
            Order.GetList = false;
        }

        internal IEnumerable<EntityMetadata> GetEntities()
        {
            Clear();
            var service = Connection.OrganizationService;

            // получить компоненты решения для уникального имени решения
            QueryExpression componentsQuery = new QueryExpression
            {
                EntityName = "solutioncomponent",
                ColumnSet = new ColumnSet("objectid"),
                Criteria = new FilterExpression(),
            };

            LinkEntity solutionLink = new LinkEntity("solutioncomponent", "solution", "solutionid", "solutionid", JoinOperator.Inner);
            solutionLink.LinkCriteria = new FilterExpression();
            solutionLink.LinkCriteria.AddCondition(new ConditionExpression("uniquename", ConditionOperator.Equal, SolutionUniqueName));
            componentsQuery.LinkEntities.Add(solutionLink);

            componentsQuery.Criteria.AddCondition(new ConditionExpression("componenttype", ConditionOperator.Equal, 1));
            Line("Выполняю запрос...");
            EntityCollection ComponentsResult = service.RetrieveMultiple(componentsQuery);
            Line("Ответ получен!");

            //var objs = ComponentsResult.Entities.Select(e => e["objectid"]).ToArray();

            Line("Создаю запрос получения всех сущностей решения");
            // Get all entities
            RetrieveAllEntitiesRequest AllEntitiesrequest = new RetrieveAllEntitiesRequest()
            {
                EntityFilters = EntityFilters.Entity | EntityFilters.Attributes,
                RetrieveAsIfPublished = true
            };
            Line("Выполняю запрос...");
            RetrieveAllEntitiesResponse AllEntitiesresponse = (RetrieveAllEntitiesResponse)service.Execute(AllEntitiesrequest);
            Line("Ответ получен!");
            //Join entities Id and solution Components Id 
            //return 
            IEnumerable<EntityMetadata> entityMetadatas = AllEntitiesresponse.EntityMetadata.Join(ComponentsResult.Entities.Select(x => x.Attributes["objectid"]), x => x.MetadataId, y => y, (x, y) => x);
            return entityMetadatas;
        }

        internal void AddToExecute(EntityMetadata entity)
        {
            executingEntities.Enqueue(entity);
        }
    }
}