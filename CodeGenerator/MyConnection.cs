﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.IO;
using System.Net;
using System.ServiceModel.Description;
using System.Text.Json;

namespace CodeGenerator
{
    public class MyConnection
    {
        const string FILENAME = "Connection.json";
        private OrganizationServiceProxy proxy;
        private ClientCredentials credentials;
        private IOrganizationService organizationService;

        public string UserName { get; internal set; } = "RMiroshnikov";
        public string Pass { get; internal set; } = "12Qwerty";
        public string Domain { get; internal set; } = "crmlearn";
        public string UriString { get; internal set; } = @"http://srv-democrm.axapta.local/LearnMiroshnikov/XRMServices/2011/Organization.svc";
        public IOrganizationService OrganizationService
        {
            get
            {
                if (organizationService == null)
                    organizationService = (IOrganizationService)proxy;
                return organizationService;
            }
        }

        internal void Connect()
        {
            credentials = new ClientCredentials();
            credentials.Windows.ClientCredential = new NetworkCredential(UserName, Pass, Domain);

            Uri uri = new Uri(UriString);
            proxy = new OrganizationServiceProxy(uri, null, credentials, null);
            proxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
        }


        public static MyConnection GetConnection()
        {
            MyConnection connection = null;
            string json = string.Empty;
            if (!File.Exists(FILENAME))
            {
                connection = new MyConnection();
                json = JsonSerializer.Serialize(connection, typeof(MyConnection), new JsonSerializerOptions { WriteIndented = true });
                File.WriteAllText(FILENAME, json);
            }
            else
            {
                json = File.ReadAllText(FILENAME);
                connection = JsonSerializer.Deserialize<MyConnection>(json);
            }
            return connection;
        }
    }
}
