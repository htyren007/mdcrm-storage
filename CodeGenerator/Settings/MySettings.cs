﻿using System;
using System.IO;
using System.Text.Json;

namespace CodeGenerator.Settings
{
    public class MySettings
    { 
        const string FILENAME = "order.json";

        public MyConnection Connection { get;  set; }
        public Order Order { get;  set; } 

        public static MySettings GetSettings()
        {
            MySettings settings = null;
            string json = string.Empty;
            if (!File.Exists(FILENAME))
            {
                settings = new MySettings();
                settings.Order = new Order();
                settings.Connection = new MyConnection();
                json = JsonSerializer.Serialize(settings, typeof(MySettings), new JsonSerializerOptions { WriteIndented = true });
                File.WriteAllText(FILENAME, json);
            }
            else
            {
                json = File.ReadAllText(FILENAME);
                settings = JsonSerializer.Deserialize<MySettings>(json);
            }
            return settings;
        }

        internal void Save()
        {
            var json = JsonSerializer.Serialize(this, typeof(MySettings), new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText(FILENAME, json);
        }
    }
}
