﻿namespace CodeGenerator.Settings
{
    public class Order
    {
        public bool GetList { get; set; } = true;

        public string List_OutputFile { get; set; } = "entities.txt";

        public bool GetMeta { get; set; } = false;

        public string[] Meta_Names { get; set; } = new[] { "account", "email", "lead" };

        public bool GenerateCSharpEntityClasses { get; set; } = false;

        public CSharpOrder CSharpOrder { get; set; } = new CSharpOrder
        {
            NameSpace = "Crm.Models",

            Classes = new[] {
                new EntityClass
                {
                    LogicalName = "lead",
                    Attributes = new[] {
                        "leadid",
                        "description",
                        "fullname",
                        "lastonholdtime",
                        "annotation",
                    },
                }
            },
        };

    }

    public class CSharpOrder
    {
        public string NameSpace { get; set; }
        public EntityClass[] Classes { get; set; }
    }

    public class EntityClass
    {
        public string LogicalName { get; set; }
        public string[] Attributes { get; set; }
    }
}
