using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
namespace Crm.Models
{
	[EntityLogicalName("annotation")]
	public class Annotation : AEntity
	{
		public const string EntityLogicalName = "annotation";
		public const int EntityTypeCode = 5;

		public Annotation() : base(EntityLogicalName) {}

		[AttributeLogicalName("annotationid")]
		public new Guid Id
		{
			get=>AnnotationId;
			set=>AnnotationId = value;
		}
		public Guid AnnotationId
		{
			get => GetValue<Guid>("annotationid");
			set => SetValue("annotationid", value);
		}

//--------------------
// Пропушен атрибут fullname
//--------------------


		[AttributeLogicalName("notetext")]
		public string NoteText
		{
			get => GetValue<string>("notetext");
			set => SetValue("notetext", value);
		}

		[AttributeLogicalName("subject")]
		public string Subject
		{
			get => GetValue<string>("subject");
			set => SetValue("subject", value);
		}

		[AttributeLogicalName("documentbody")]
		public string DocumentBody
		{
			get => GetValue<string>("documentbody");
			set => SetValue("documentbody", value);
		}
	}
}
