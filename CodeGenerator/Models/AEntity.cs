// Базовый тип
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

[assembly: ProxyTypesAssembly()]
namespace Crm.Models
{
	[DataContract]
	public class AEntity : Entity, INotifyPropertyChanging, INotifyPropertyChanged
	{
        public AEntity(string entityName) : base(entityName){}

        public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName = null)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		protected void OnPropertyChanging(string propertyName = null)
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
			}
		}

		protected void SetValue(string attributeLogicalName, object value, [CallerMemberName] string propertyName = null)
		{
			OnPropertyChanging(propertyName);
			SetAttributeValue(attributeLogicalName, value);
			OnPropertyChanged(propertyName);
		}

		protected T GetValue<T>(string attributeLogicalName)
        {
			return this.GetAttributeValue<T>(attributeLogicalName);
		}
	}
}
