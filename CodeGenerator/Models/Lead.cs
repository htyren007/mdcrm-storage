using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
namespace Crm.Models
{
	[EntityLogicalName("lead")]
	public class Lead : AEntity
	{
		public const string EntityLogicalName = "lead";
		public const int EntityTypeCode = 4;

		public Lead() : base(EntityLogicalName) {}

		[AttributeLogicalName("leadid")]
		public new Guid Id
		{
			get=>LeadId;
			set=>LeadId = value;
		}
		public Guid LeadId
		{
			get => GetValue<Guid>("leadid");
			set => SetValue("leadid", value);
		}

		[AttributeLogicalName("description")]
		public string Description
		{
			get => GetValue<string>("description");
			set => SetValue("description", value);
		}

		[AttributeLogicalName("fullname")]
		public string FullName
		{
			get => GetValue<string>("fullname");
			set => SetValue("fullname", value);
		}

		[AttributeLogicalName("lastonholdtime")]
		public DateTime LastOnHoldTime
		{
			get => GetValue<DateTime>("lastonholdtime");
			set => SetValue("lastonholdtime", value);
		}

//--------------------
// Пропушен атрибут annotations
//--------------------

	}
}
