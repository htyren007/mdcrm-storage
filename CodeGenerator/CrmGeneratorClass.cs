﻿using CodeGenerator.Settings;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Linq;
using System.Text;

namespace CodeGenerator
{
    public class CrmGeneratorClass
    {
        public static string BaseClass(string nameSpace)
        {
            return $@"// Базовый тип
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

[assembly: ProxyTypesAssembly()]
namespace {nameSpace}
{{
	[DataContract]
	public class AEntity : Entity, INotifyPropertyChanging, INotifyPropertyChanged
	{{
        public AEntity(string entityName) : base(entityName){{}}

        public event PropertyChangingEventHandler PropertyChanging;
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName = null)
		{{
			if (this.PropertyChanged != null)
			{{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}}
		}}

		protected void OnPropertyChanging(string propertyName = null)
		{{
			if (this.PropertyChanging != null)
			{{
				this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
			}}
		}}

		protected void SetValue(string attributeLogicalName, object value, [CallerMemberName] string propertyName = null)
		{{
			OnPropertyChanging(propertyName);
			SetAttributeValue(attributeLogicalName, value);
			OnPropertyChanged(propertyName);
		}}

		protected T GetValue<T>(string attributeLogicalName)
        {{
			return this.GetAttributeValue<T>(attributeLogicalName);
		}}
	}}
}}
";
        }

        public static string Generate(EntityMetadata meta, CSharpOrder order)
        {
            var orderClass = order.Classes.FirstOrDefault(cl => cl.LogicalName == meta.LogicalName);
            if (orderClass == null)
                return null;

            var orderArrtibute = orderClass.Attributes.ToList();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;");
            builder.AppendLine($"namespace {order.NameSpace}");
            builder.AppendLine("{");


            builder.AppendLine($"\t[EntityLogicalName(\"{meta.LogicalName}\")]");
            builder.AppendLine($"\tpublic class {meta.SchemaName} : AEntity");
            builder.AppendLine("\t{");

            builder.AppendLine($"\t\tpublic const string EntityLogicalName = \"{meta.LogicalName}\";");
            if (meta.ObjectTypeCode.HasValue)
                builder.AppendLine($"\t\tpublic const int EntityTypeCode = {meta.ObjectTypeCode.Value};\n");

            builder.AppendLine($"\t\tpublic {meta.SchemaName}() : base(EntityLogicalName) {{}}\n");

            #region Id
            string idAttribute = meta.PrimaryIdAttribute;
            var idAtt = meta.Attributes.FirstOrDefault(a => a.LogicalName == idAttribute);
            builder.AppendLine($"\t\t[AttributeLogicalName(\"{idAtt.LogicalName}\")]");
            builder.AppendLine("\t\tpublic new Guid Id");
            builder.AppendLine("\t\t{");
            builder.AppendLine($"\t\t\tget=>{idAtt.SchemaName};");
            builder.AppendLine($"\t\t\tset=>{idAtt.SchemaName} = value;");
            builder.AppendLine("\t\t}");

            builder.AppendLine($"\t\tpublic Guid {idAtt.SchemaName}");
            builder.AppendLine("\t\t{");
            builder.AppendLine($"\t\t\tget => GetValue<Guid>(\"{idAtt.LogicalName}\");");
            builder.AppendLine($"\t\t\tset => SetValue(\"{idAtt.LogicalName}\", value);");
            builder.AppendLine("\t\t}");
            orderArrtibute.Remove(idAtt.LogicalName);
            #endregion

            foreach (var attributeName in orderArrtibute)
            {
                var attribute = meta.Attributes.FirstOrDefault(a => a.LogicalName == attributeName);
                if (attribute != null)
                    GenerateArrtubute(attribute, builder);
                else
                    builder.AppendLine($@"
//--------------------
// Пропушен атрибут {attributeName}
//--------------------
");
            }
            builder.AppendLine("\t}");
            builder.AppendLine("}");

            return builder.ToString();
        }

        private static void GenerateArrtubute(AttributeMetadata meta, StringBuilder builder)
        {
            string type = GetTypeString(meta.AttributeType);
            builder.AppendLine($"\n\t\t[AttributeLogicalName(\"{meta.LogicalName}\")]");
            builder.AppendLine($"\t\tpublic {type} {meta.SchemaName}");
            builder.AppendLine("\t\t{");
            builder.AppendLine($"\t\t\tget => GetValue<{type}>(\"{meta.LogicalName}\");");
            builder.AppendLine($"\t\t\tset => SetValue(\"{meta.LogicalName}\", value);");
            builder.AppendLine("\t\t}");
        }

        private static string GetTypeString(AttributeTypeCode? attributeType)
        {
            switch (attributeType)
            {
                case AttributeTypeCode.Uniqueidentifier: return "Guid";
                case AttributeTypeCode.Boolean: return "bool";
                case AttributeTypeCode.DateTime: return "DateTime";
                case AttributeTypeCode.Decimal: return "decimal";
                case AttributeTypeCode.Integer: return "int";
                case AttributeTypeCode.Double: return "double";
                case AttributeTypeCode.String: return "string";
                case AttributeTypeCode.Memo: return "string";

                default:
                    throw new NotImplementedException($"Метод или операция не реализована {attributeType}");
            }
        }
    }
}