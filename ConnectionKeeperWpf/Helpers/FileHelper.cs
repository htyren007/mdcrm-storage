﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConnectionKeeperWpf
{
    public static class FileHelper
    {
        public static readonly string DATA_FILENAME = "data";
        public static readonly string CUSTOMERS_FILENAME = Path.Combine(DATA_FILENAME, "customers.json");
        public static readonly string YOTA_SETTINGS_FILENAME = Path.Combine(DATA_FILENAME, "settings.json");

        public static bool ExistsCustomers() => File.Exists(CUSTOMERS_FILENAME);
        public static List<CustomerDto> GetCustumers()
        {
            if (ExistsCustomers())
            {
                var json = File.ReadAllText(CUSTOMERS_FILENAME);
                List<CustomerDto> result = JsonConvert.DeserializeObject<List<CustomerDto>>(json);
                return result;
            }
            return new List<CustomerDto>();
        }
        public static void SaveCustumers(IEnumerable<CustomerDto> customers)
        {
            Directory.CreateDirectory(DATA_FILENAME);
            var json = JsonConvert.SerializeObject(customers);
            File.WriteAllText(CUSTOMERS_FILENAME, json);
        }

        public static bool ExistsSettings() => File.Exists(YOTA_SETTINGS_FILENAME);
        public static MainSettings GetSettings()
        {
            if (ExistsSettings())
            {
                var json = File.ReadAllText(YOTA_SETTINGS_FILENAME);
                return JsonConvert.DeserializeObject<MainSettings>(json);
            }
            return new MainSettings();
        }
        public static void SaveSettings(MainSettings settings)
        {
            Directory.CreateDirectory(DATA_FILENAME);
            var json = JsonConvert.SerializeObject(settings);
            File.WriteAllText(YOTA_SETTINGS_FILENAME, json);
        }
    }
}