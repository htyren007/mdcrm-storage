﻿namespace ConnectionKeeperWpf
{
    public struct ConstString
    {
        public const string DefaultStatus = "неопределён";
        public const string SettingTitle = "Connection Keeper - Настройки";
        public const string SettingOpenCitrix = "Connection Keeper - Запуск цитрикса";
        public const string MessageTitle = "Connection Keeper - Сообщение";
        public const string StatusConnect = "есть контакт";
        public const string StatusDisconnect = "нет соединения";
        public const string RequestsTitle = "Connection Keeper - Запросы";
        public const string DefaultStatus1 = "";
    }
}
