﻿using WPFStorage.Base;

namespace ConnectionKeeperWpf
{
    public class CustomerModel : ObservableObject
    {
        private string displayName;
        private string customerId;
        private string msisdn;
        // private CustomerDto dto;

        public CustomerModel(CustomerDto dto)
        {
            displayName = dto.DisplayName;
            customerId = dto.CustomerId;
            msisdn = dto.Msisdn;
        }

        public string DisplayName { get => displayName; set => SetProperty(ref displayName, value); }
        public string CustomerId { get => customerId; set => SetProperty(ref customerId, value); }
        public string Msisdn { get => msisdn; set => SetProperty(ref msisdn, value); }

        public CustomerDto GetDto()
        {
            return new CustomerDto
            {
                DisplayName = displayName,
                CustomerId = customerId,
                Msisdn = msisdn,
            };
        }
    }
}