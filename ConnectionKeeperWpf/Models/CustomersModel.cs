﻿using hty.Rest.Yota;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using WPFStorage.Base;
using WPFStorage.Dialogs;
using YotaRest.Requests;
using YotaRest.Services;

namespace ConnectionKeeperWpf
{

    internal class CustomersModel : ObservableObject
    {
        private CustomerModel selectCustomer;

        public CustomersModel()
        {
            List<CustomerDto> cusdto = FileHelper.GetCustumers();
            var customers = cusdto.Select(x => new CustomerModel(x));
            Custumers = new ObservableCollection<CustomerModel>(customers);
            AddCommand = new RelayCommand(Add);
            SaveCommand = new RelayCommand(Save);
            DeleteCommand = new RelayCommand(Delete);
            UpdateNameCommand = new RelayCommand(UpdateName);

            GetSimcardsCommand = new RelayCommand(GetSimcards);
            ProductAvailableCommand = new RelayCommand(ProductAvailable);
            FeaturesCustomerCommand = new RelayCommand(FeaturesCustomer);
            IdentifiersCommand = new RelayCommand(Identifiers);
            SelectCustomer = Custumers.First();
        }

        private void UpdateName()
        {
            YotaClient yota = new YotaClient(FileHelper.GetSettings());
            foreach (var customer in Custumers)
            {
                var identifiers = yota.RIDS.Identifiers().KnownId(customer.CustomerId).KnownIdType(IdType.customerId);
                if (identifiers.Execute())
                {
                    customer.Msisdn = identifiers.Result.msisdn;
                }

                var pd = yota.CISRest.CustomerPd().Msisdn(customer.Msisdn);
                if (pd.Execute())
                {
                    var name = pd.Result.name;
                    if (name != null)
                        customer.DisplayName = $"{name.firstName} {name.lastName}";
                    //else
                    //    customer.DisplayName = ";
                }
            }
            Save();
        }

        private void Identifiers()
        {
            RestRequest request = new RestRequest();
            request.PathRequest = "http://ft2-ec-api.g4lab.com:8080/ec/v1/identifiers";

            request.QueryParams.Add(new RequestParamModel { Key = "knownId", Value = SelectCustomer.CustomerId });
            request.QueryParams.Add(new RequestParamModel { Key = "knownIdType", Value = "CUSTOMER_ID" });
            // request.HeaderParams.Add(new RequestParamModel { Key = "accept", Value = "application/json" });
            request.Open();
        }

        private void FeaturesCustomer()
        {
            if (SelectCustomer == null)
            {
                WinBox.ShowMessage("Сначала выберете пользователя!");
                return;
            }
            RestRequest request = new RestRequest();
            var settings = FileHelper.GetSettings();
            request.PathRequest = $"{settings.Pm}/v1/features/customer/{{customerId}}";
            request.PathParams.Add(new RequestParamModel { Key = "customerId", Value = SelectCustomer.CustomerId });
            request.QueryParams.Add(new RequestParamModel { Key = "key", Value = "unlimPkg" });
            request.Open();
        }

        private void ProductAvailable()
        {
            if (SelectCustomer == null)
            {
                WinBox.ShowMessage("Сначала выберете пользователя!");
                return;
            }
            RestRequest request = new RestRequest();
            var settings = FileHelper.GetSettings();
            request.PathRequest = $"{settings.Pm}/v2/products/available";
            request.QueryParams.Add(new RequestParamModel { Key = "customerId", Value = SelectCustomer.CustomerId });
            request.QueryParams.Add(new RequestParamModel { Key = "accType", Value = "B2C" });
            request.QueryParams.Add(new RequestParamModel { Key = "filter", Value = "allin" });
            request.QueryParams.Add(new RequestParamModel { Key = "salesChannel", Value = "CRM" });
            request.Open();

            
        }

        private void GetSimcards()
        {
            if (SelectCustomer == null)
            {
                WinBox.ShowMessage("Сначала выберете пользователя!");
                return;
            }
            RestRequest request = new RestRequest();
            var settings = FileHelper.GetSettings();
            request.PathRequest = $"{settings.Pm}/v1/simcards";
            request.QueryParams.Add(new RequestParamModel { Key = "customerId", Value = SelectCustomer.CustomerId });
            request.QueryParams.Add(new RequestParamModel { Key = "returnProducts", Value = "true" });
            request.QueryParams.Add(new RequestParamModel { Key = "returnDiscounts", Value = "true" });
            request.QueryParams.Add(new RequestParamModel { Key = "returnPrices", Value = "true" });
            request.Open();

            
        }

        private void Delete()
        {
            if (SelectCustomer != null)
            {
                Custumers.Remove(SelectCustomer);
            }
        }

        private void Save()
        {
            FileHelper.SaveCustumers(Custumers.Select(x => x.GetDto()));
        }

        private void Add()
        {
            var customer = new CustomerModel(new CustomerDto { DisplayName = "Новая запись" });

            Custumers.Add(customer);
            SelectCustomer = customer;
        }

        public CustomerModel SelectCustomer { get => selectCustomer; set => SetProperty(ref selectCustomer, value); }
        public ObservableCollection<CustomerModel> Custumers { get; set; }
        public RelayCommand AddCommand { get; }
        public RelayCommand SaveCommand { get; }
        public RelayCommand DeleteCommand { get; }
        public RelayCommand GetSimcardsCommand { get; }
        public RelayCommand ProductAvailableCommand { get; }
        public RelayCommand FeaturesCustomerCommand { get; }
        public RelayCommand IdentifiersCommand { get; }
        public RelayCommand UpdateNameCommand { get; }
    }
}