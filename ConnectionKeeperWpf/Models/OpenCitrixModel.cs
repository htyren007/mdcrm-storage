﻿using System;
using System.Diagnostics;
using System.Linq;
using WPFStorage.Base;
using WPFStorage.Dialogs;

namespace ConnectionKeeperWpf
{
    public class OpenCitrixModel: ObservableObject
    {
        private OpenCitrixWindow window;
        private string password;
        private string login;

        public RelayCommand CloseCommand { get; }
        public RelayCommand CopyLoginCommand { get; }
        public RelayCommand CopyPassCommand { get; }
        public RelayCommand StartCitrixCommand { get; }
        public RelayCommand StopCitrixCommand { get; }
        public string Login { get => login; set => SetProperty(ref login, value); }
        public string Password { get => password; set => SetProperty(ref password, value); }

        public OpenCitrixModel(OpenCitrixWindow window)
        {
            this.window = window;
            CloseCommand = new RelayCommand(CloseWindow);
            CopyLoginCommand = new RelayCommand(CopyLogin);
            CopyPassCommand = new RelayCommand(CopyPass);
            StartCitrixCommand = new RelayCommand(StartCitrix);
            StopCitrixCommand = new RelayCommand(StopCitrix);
            Login = Properties.Settings.Default.Login;
            Password = Properties.Settings.Default.Password;
            StartCitrix();

        }

        private void StopCitrix()
        {
            try
            {
                var procNames = Properties.Settings.Default.Processes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var processes = Process.GetProcesses();
                int count = 0;
                foreach (Process proc in processes)
                {
                    if (procNames.Any(x => proc.ProcessName.Contains(x)))
                    {
                        proc.Kill();
                        count++;
                    }
                }
                WinBox.ShowMessage($"Завершено {count} процессов!", ConstString.MessageTitle);
            }
            catch (Exception ex)
            {
                WinBox.ShowMessage($"{ex.GetType().Name}: {ex.Message}");
            }
        }

        private static void StartCitrix()
        {
            var citrixName = Properties.Settings.Default.Processes;

            try
            {
                var processes = Process.GetProcesses();
                var citrixProcess = processes.FirstOrDefault(x => x.StartInfo.FileName == citrixName);
                if (citrixProcess == null)
                {
                    var process = Process.Start(citrixName);
                }
                else { 
                    
                }

            }
            catch (Exception ex)
            {
                WinBox.ShowMessage($"{ex.GetType().Name}: {ex.Message}");
            }
        }

        private void CopyPass()
        {
            WinBox.ShowMessage($"CopyPass");
        }

        private void CopyLogin()
        {
            WinBox.ShowMessage($"CopyLogin");
        }

        private void CloseWindow()
        {
            window.Close();
        }
    }
}