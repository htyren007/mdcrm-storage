﻿using System;
using WPFStorage.Base;
using WPFStorage.Dialogs;
using WPFStorage.Settings;

namespace ConnectionKeeperWpf
{
    public class MainModel : ObservableObject
    {
        private MainWindow window;
        private Keeper keeper;
        private string status;
        private bool needButtonOpenCitrix = true;

        public RelayCommand OpenCitrixCommand { get; set; }
        public RelayCommand CustomersCommand { get; }
        public RelayCommand OpenSettingsCommand { get; }
        public bool NeedButtonOpenCitrix 
        { 
            get => needButtonOpenCitrix; 
            set => SetProperty(ref needButtonOpenCitrix, value); 
        }
        public string Status { get => status; set => SetProperty(ref status, value); }
        public event Action<KeeperStatus> OnChangeStatus;

        public MainModel(MainWindow win)
        {
            Status = ConstString.DefaultStatus;
            window = win;
            keeper = new Keeper();
            keeper.OnChangeStatus += StatusChenged;

            OpenCitrixCommand = new RelayCommand(OpenCitrixHeader);
            CustomersCommand = new RelayCommand(OpenCustomers);
            OpenSettingsCommand = new RelayCommand(OpenSettings);
            CreateSettings();
            StatusChenged(keeper.Status);

            // OpenCustomers();
            //OpenRequest();
        }

        private void OpenRequest()
        {
            RestRequest request = new RestRequest();
            request.PathRequest = "http://ft2-ec-api.g4lab.com:8080/ec/v1/identifiers";
            
            request.QueryParams.Add(new RequestParamModel { Key = "knownId", Value = "965c6dca76114dd3b2c2c1dec4561b50" });
            request.QueryParams.Add(new RequestParamModel { Key = "knownIdType", Value = "CUSTOMER_ID" });
            // request.HeaderParams.Add(new RequestParamModel { Key = "accept", Value = "application/json" });
            request.Open();

        }

        private void CreateSettings()
        {
            //if (!FileHelper.ExistsSettings())
            if (true)
            {
                FileHelper.SaveSettings(new MainSettings { 
                    Pes = "http://ft2-pm-api.g4lab.com:8080/pm/v1",
                    Pm = "http://ft2-pm-api.g4lab.com:8080/pm",
                    Dms = "http://ft2-pm-api.g4lab.com:8080/pm/v1",
                    Rids = "http://ft2-ec-api.g4lab.com:8080/ec/v1",
                    CisRest = "http://ft2-cd-api.g4lab.com:8080/cdm/v1",
                    Cids = "http://ft2-cd-api.g4lab.com:8080/cdm/v1",
                });
            }
        }

        private void StatusChenged(KeeperStatus newStatus)
        {
            Status = newStatus == KeeperStatus.Connect 
                ? ConstString.StatusConnect : newStatus == KeeperStatus.Disconnect 
                ? ConstString.StatusDisconnect : ConstString.DefaultStatus;
            NeedButtonOpenCitrix = newStatus != KeeperStatus.Connect;
            OnChangeStatus?.Invoke(newStatus);
        }

        private void OpenSettings()
        {
            var setwin = new Settings();
            setwin.Title = ConstString.SettingTitle;
            setwin.AddString("Processes", "Процессы цитрикса").SetValue(Properties.Settings.Default.Processes)
                .OnChange((v)=> Properties.Settings.Default.Processes = v);
            setwin.AddString("CitrixName", "Путь до цитрикса").SetValue(Properties.Settings.Default.CitrixName)
                .OnChange((v) => Properties.Settings.Default.CitrixName = v);
            setwin.AddString("Login", "Логин").SetValue(Properties.Settings.Default.Login)
                .OnChange((v) => Properties.Settings.Default.Login = v);
            setwin.AddString("Password", "Пароль").SetValue(Properties.Settings.Default.Password)
                .OnChange((v) => Properties.Settings.Default.Password = v);

            setwin.Open((s)=> {
                if (s.Result)
                {
                    Properties.Settings.Default.Save();
                }
            });
        }

        private void OpenCustomers()
        {
            CustomersWindow window = new CustomersWindow();
            window.Title = ConstString.RequestsTitle;
            window.DataContext = new CustomersModel();
            window.Show();
            //WinBox.ShowMessage("Открытие окна отправки запроса");
        }

        private void OpenCitrixHeader()
        {
            OpenCitrixWindow win = new OpenCitrixWindow();
            win.Title = ConstString.SettingOpenCitrix;
            win.Show();
        }
    }
}
