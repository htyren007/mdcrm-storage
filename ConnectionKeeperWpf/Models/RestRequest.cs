﻿using ConnectionKeeperWpf.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using WPFStorage.Base;
using WPFStorage.Dialogs;
using YotaRest.Requests;

namespace ConnectionKeeperWpf
{
    public class RestRequest : ObservableObject
    {
        private RequestParamModel selectedPathParam;
        private RequestParamModel selectedQueryParam;
        private RequestParamModel selectedHeaderParam;
        private string methodType;
        private string path;
        private bool wasSended;
        private RestRequestWindow window;

        public RestRequest()
        {
            AddPathParamCommand = new RelayCommand(AddPathParam);
            DeletePathParamCommand = new RelayCommand<RequestParamModel>(DeletePathParam);
            AddQuereParamCommand = new RelayCommand(AddQuereParam);
            DeleteQuereParamCommand = new RelayCommand<RequestParamModel>(DeleteQuereParam);
            AddHeaderParamCommand = new RelayCommand(AddHeaderParam);
            DeleteHeaderParamCommand = new RelayCommand<RequestParamModel>(DeleteHeaderParam);
            SendCommand = new RelayCommand(Send);
            MetodTypes = new ObservableCollection<string>(new[] { "get", "post" });
            Method = MetodTypes.FirstOrDefault();
        }

        private void Send()
        {
            UniversalRestRequest request = new UniversalRestRequest(PathRequest);
            request.Method = Method == "get" ? HttpMethod.Get : HttpMethod.Post;
            foreach (var param in PathParams)
            {
                request.PathParam(param.Key, param.Value);
            }
            foreach (var param in QueryParams)
            {
                request.QueryParam(param.Key, param.Value);
            }
            foreach (var param in HeaderParams)
            {
                request.HeaderParam(param.Key, param.Value);
            }
            request.Send();
            if (!request.IsError)
            {
                window.JsonPresenter.Load(request.Response);
                WasSended = true;
                window.tabs.SelectedIndex = 1;
            }
            // WinBox.ShowMessage("Это программиты ещё не предусматрели!");
        }

        public ObservableCollection<string> MetodTypes { get; set; }
        public ObservableCollection<RequestParamModel> QueryParams { get; set; } = new ObservableCollection<RequestParamModel>();
        public ObservableCollection<RequestParamModel> PathParams { get; set; } = new ObservableCollection<RequestParamModel>();
        public ObservableCollection<RequestParamModel> HeaderParams { get; set; } = new ObservableCollection<RequestParamModel>();

        public string PathRequest
        {
            get => path;
            set => SetProperty(ref path, value);
        }
        public bool WasSended
        {
            get => wasSended;
            set => SetProperty(ref wasSended, value);
        }
        public string Method
        {
            get => methodType;
            set => SetProperty(ref methodType, value);
        }
        public RequestParamModel SelectedQueryParam
        {
            get => selectedQueryParam;
            set => SetProperty(ref selectedQueryParam, value);
        }
        public RequestParamModel SelectedPathParam
        {
            get => selectedPathParam;
            set => SetProperty(ref selectedPathParam, value);
        }
        public RequestParamModel SelectedHeaderParam
        {
            get => selectedHeaderParam;
            set => SetProperty(ref selectedHeaderParam, value);
        }

        public RelayCommand AddPathParamCommand { get; }
        public RelayCommand<RequestParamModel> DeletePathParamCommand { get; }
        public RelayCommand AddQuereParamCommand { get; }
        public RelayCommand<RequestParamModel> DeleteQuereParamCommand { get; }
        public RelayCommand AddHeaderParamCommand { get; }
        public RelayCommand<RequestParamModel> DeleteHeaderParamCommand { get; }
        public RelayCommand SendCommand { get; }


        #region Methods
        private void DeleteHeaderParam(RequestParamModel obj)
        {
            HeaderParams.Remove(SelectedHeaderParam);
            SelectedHeaderParam = HeaderParams.LastOrDefault();
        }

        private void AddHeaderParam()
        {
            var param = new RequestParamModel() { Key = "Новый", Value = "value" };
            HeaderParams.Add(param);
            SelectedHeaderParam = param;
        }

        private void DeletePathParam(RequestParamModel obj)
        {
            PathParams.Remove(SelectedPathParam);
            SelectedPathParam = PathParams.LastOrDefault();
        }

        private void AddPathParam()
        {
            var param = new RequestParamModel() { Key = "Новый", Value = "value" };
            PathParams.Add(param);
            SelectedPathParam = param;
        } 
        private void DeleteQuereParam(RequestParamModel obj)
        {
            QueryParams.Remove(SelectedQueryParam);
            SelectedQueryParam = QueryParams.LastOrDefault();
        }

        private void AddQuereParam()
        {
            var param = new RequestParamModel() { Key = "Новый", Value = "value" };
            QueryParams.Add(param);
            SelectedQueryParam= param;
        }

        public void Open()
        {
            window = new Views.RestRequestWindow();
            window.DataContext = this;
            window.ShowDialog();
        }
        #endregion

    }
}
