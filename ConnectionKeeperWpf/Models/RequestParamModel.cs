﻿using System;
using WPFStorage.Base;

namespace ConnectionKeeperWpf
{
    public class RequestParamModel : ObservableObject
    {
        private string displayName;
        private string key;
        private string _value;

        public RequestParamModel()
        {
            
        }

        public string DisplayName
        {
            get => displayName;
            private set => SetProperty(ref displayName, value);
        }
        public string Key
        {
            get => key;
            set { SetProperty(ref key, value); SetDisplayName(); }
        }

        public string Value
        {
            get => _value;
            set { SetProperty(ref _value, value); SetDisplayName(); }
        }

        private void SetDisplayName()
        {
            DisplayName = $"{key}:{_value}";
        }
    }
}
