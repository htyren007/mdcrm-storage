﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ConnectionKeeperWpf
{
    public class Keeper
    {
        private KeeperStatus status;
        private DispatcherTimer timer;

        public KeeperStatus Status
        {
            get => status; 
            set
            {
                status = value;
                OnChangeStatus(status);
            }
        }
        public event Action<KeeperStatus> OnChangeStatus;
        public Keeper()
        {
            timer = new DispatcherTimer();
            timer.Tick += Work;
            timer.Interval = new TimeSpan(0, 0, (int)Properties.Settings.Default.Interval);
            timer.Start();
            Task.Run(async ()=>{
                await Task.Delay(3000);
                Work(null, null);
                //timer.Interval = new TimeSpan(0, 0, (int)Properties.Settings.Default.Interval);
                // timer.IsEnabled = true;
            });
        }

        private void Work(object sender, EventArgs e)
        {
            timer.Stop();
            GetRequest();
            timer.Start();
        }

        private void GetRequest()
        {
            var uri = "http://ft2-cd-api.g4lab.com:8080/cdm/v1/customer/14c3f351754049f09f2172d6382f714b/identification";
            HttpWebRequest http = WebRequest.CreateHttp(uri);
            http.Method = HttpMethod.Get.Method;
            DefaultHeaders(http);
            try
            {
                //http.Timeout = 100000;
                using (HttpWebResponse response = (HttpWebResponse)(http.GetResponse()))
                {
                    //if (Settings.Instance.General.IsSaveResponses)
                    //{
                        using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
                        {
                            //File.AppendAllText("log.txt", $"\n===== {DateTime.Now} =====\n" + rdr.ReadToEnd());
                        }
                    //}
                    //if (messageAfterRequest)
                    //    Text($"Успешный запрос {++Index}!");
                    Status = KeeperStatus.Connect;
                }
            }
            catch (Exception ex)
            {
                //if (messageAfterRequest)
                //{
                //    Line();
                //    Line($"Ошибка запроса {++Index}. ");
                //    Error(ex);
                //}
                //if (Settings.Instance.General.StopAfterError)
                //    Stop();
                //File.AppendAllText("log.txt",
                //    $"\n===== Ошибка запроса {Index}: {DateTime.Now} =====\n{ex.GetType().Name}: {ex.Message}\nuri: {uri}\n");
                if (ex is WebException webEx)
                {
                    if (webEx.Status == WebExceptionStatus.ConnectFailure)
                        Status = KeeperStatus.Disconnect;
                }
            }
        }

        private void DefaultHeaders(HttpWebRequest http)
        {
            http.UserAgent = ".Net ConnectionKeeper";
            http.Accept = "*/*";
        }
    }
}