﻿namespace ConnectionKeeperWpf
{
    public class CustomerDto
    {
        public string DisplayName { get; set; }
        public string CustomerId { get; set; }
        public string Msisdn { get; set; }
    }
}