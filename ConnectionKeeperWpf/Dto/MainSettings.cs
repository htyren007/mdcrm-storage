﻿using hty.Rest.Yota;

namespace ConnectionKeeperWpf
{
    public class MainSettings : IYotaSettings
    {
        public string Pes { get; set; }
        public string Dms { get; set; }
        public string Rids { get; set; }
        public string CisRest { get; set; }
        public string Cids { get; set; }
        public string Pm { get; set; }
    }
}