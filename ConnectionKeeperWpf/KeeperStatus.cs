﻿namespace ConnectionKeeperWpf
{
    public enum KeeperStatus
    {
        Unknown,
        Connect,
        Disconnect
    }
}