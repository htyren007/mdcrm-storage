﻿using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.WebApi.Patch;
using Microsoft.VisualStudio.Services.WebApi.Patch.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace JiraIntegation
{
    internal class WorkItemTrackingSimple
    {
        const string TFS_URL = @"https://crm-tfs.gmcs.ru/tfs/CRM";
        private static string username = "rmiroshnikov@gmcs.ru";
        private static string password = "0h5t75";




        // private static string username = "afedyaeva@gmcsazure.onmicrosoft.com";
        //private static string username = "afedyaeva@gmcs.ru";
        //private static string password = "3Fyfcnfcbz3261998";
        private static string teamProjectName = "Yota";


        internal static async Task Run()
        {
            await FindTask();
            await GetTask();

            // await CreateTask();

            Console.ReadLine();
        }

        private static async Task GetTask()
        {
            var client = new WorkItemTrackingHttpClient(
                            new Uri(TFS_URL),
                            new VssCredentials(new WindowsCredential(new NetworkCredential(username, password))));

            var work = await client.GetWorkItemAsync(80557);

            WriteTask(work);
        }

        private static async Task CreateTask()
        {
            var client = new WorkItemTrackingHttpClient(
                                        new Uri(TFS_URL),
                                        new VssCredentials(new WindowsCredential(new NetworkCredential(username, password))));

            JsonPatchDocument document = new JsonPatchDocument();
            
            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/Microsoft.VSTS.Common.Activity",
                Value = "Development"
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.Title",
                Value = "CE-0000: Renat Test"
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.Description",
                Value = "Это тест просто тест его надо удалить!"
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.TeamProject",
                Value = "Yota"
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.IterationPath",
                Value = @"Yota\ТП 1 кв.2022"
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/GMCS.Priority",
                Value = "3. Средний"
            });

            //document.Add(new JsonPatchOperation
            //{
            //    From = null,
            //    Operation = Operation.Add,
            //    Path = "/fields/System.AssignedTo",
            //    Value = "rmiroshnikov@gmcs.ru"
            //});

            var work = await client.CreateWorkItemAsync(document, teamProjectName, "Task");
            WriteTask(work);

        }

        private static async Task FindTask()
        {
            var client = new WorkItemTrackingHttpClient(
                            new Uri(TFS_URL),
                            new VssCredentials(new WindowsCredential(new NetworkCredential(username, password))));

            var title = "CE-6223";
            var result = await client.QueryByWiqlAsync(new Wiql()
            {
                Query = @$"
Select [System.Id], [System.Title], [System.State] 
From WorkItems 
Where [System.WorkItemType] = 'Task' 
    And [System.Title] Contains '{title}' 
order by [System.CreatedDate] desc"
            });

            if (result.WorkItems.Any())
            {
                var href = result.WorkItems.First();
                var task = await client.GetWorkItemAsync(href.Id);

                WriteTask(task);
            }
            else
            {
                Line("Записи не обнаружены!");
            }
        }

        private static void WriteTask(WorkItem work)
        {
            Line("id:", work.Id);
            Line("rev:", work.Rev);
            Line("Title:", work.Fields["System.Title"]);
            Line("Description:", work.Fields["System.Description"]);
            Space();
            var type = work.Fields["System.WorkItemType"];
            Line("WorkItemType:", type);

            var teamProject = work.Fields["System.TeamProject"];
            Line("TeamProject:", teamProject);

            var path = work.Fields["System.IterationPath"];
            Line("IterationPath:", path);

            var priority = work.Fields["GMCS.Priority"];
            Line("Priority:", priority);

            var activity = work.Fields["Microsoft.VSTS.Common.Activity"];
            Line("Activity:", activity);
        }
        private static void Line(params object[] messages) => Console.WriteLine(string.Join(" ", messages));
        private static void Text(object message) => Console.Write(message.ToString());
        private static void Space() => Console.WriteLine();
    }
}