﻿using JiraTfsIntegation.Tfs.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JiraTfsIntegation.Tfs
{
    public class TfsApiClient
    {
        private string instance;
        private string username;
        private string password;

        public TfsApiClient(string instance)
        {
            this.instance = instance;
        }

        public void SetCredentials(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public async Task<WorkItem> FindTaskByTitle(string title, int top = 10)
        {
            try
            {
                Uri uri = new Uri($"https://{instance}/_apis/wit/wiql?$top={top}&api-version=3.2");
                var response = await PostWiqlAsync<TfsWiqlResponse>(uri, $"Select [System.Id], [System.Title], [System.State] From WorkItems Where [System.WorkItemType] = 'Task' And [System.Title] Contains '{title}' order by [System.CreatedDate] desc");
                if (response.workItems.Count > 0)
                    return await GetWorkItem(response.workItems[0]);
            }
            catch (WebException webEx)
            {
                // TODO: Возможно следует включить обработку ошибок авторизации!
                //if (webEx.Status == WebExceptionStatus.ProtocolError)
                throw;
            }
            return null;
        }

        public async Task<TfsWiqlResponse> GetBagsAsync(string project, int top = 50)
        {
            Uri uri = new Uri($"https://{instance}/_apis/wit/wiql?$top={top}&api-version=3.2");
            return await PostWiqlAsync<TfsWiqlResponse>(uri, "Select [System.Id], [System.Title], [System.State] From WorkItems Where [System.WorkItemType] = 'Task' order by [System.CreatedDate] desc");
        }

        public async Task<WorkItem> GetWorkItem(WorkItemLink item) => await GetAsync<WorkItem>(item.url);

        #region Private methods
        private async Task<TResponse> GetAsync<TResponse>(string uri) => await GetAsync<TResponse>(new Uri(uri));

        private async Task<TResponse> GetAsync<TResponse>(Uri uri)
        {
            try
            {
                HttpWebRequest http = CreateHttp(uri, HttpMethod.Get);
                SetCreditals(http);
                return await GetResponseAndParse<TResponse>(http);

            }
            catch (System.Net.Http.HttpRequestException httpReqEx)
            {
                throw;
            }
            catch (WebException webEx)
            {
                throw;
            }
        }

        private async Task<TResponse> PostWiqlAsync<TResponse>(Uri uri, string query)
        {
            try
            {
                HttpWebRequest http = CreateHttp(uri, HttpMethod.Post);
                SetCreditals(http);

                var content = string.Format(@"{{""query"":""{0}""}}", query);
                byte[] postBytes = Encoding.UTF8.GetBytes(content);
                http.ContentLength = postBytes.Length;
                http.ContentType = "application/json; charset=UTF-8";
                using (Stream requestStream = http.GetRequestStream())
                {
                    requestStream.Write(postBytes, 0, postBytes.Length);
                    requestStream.Close();
                }

                return await GetResponseAndParse<TResponse>(http);
            }
            catch (System.Net.Http.HttpRequestException httpReqEx)
            {
                throw;
            }
            catch (WebException webEx)
            {
                throw;
            }
        }

        private static async Task<TResponse> GetResponseAndParse<TResponse>(HttpWebRequest http)
        {
            HttpWebResponse response = (HttpWebResponse)(await http.GetResponseAsync());
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            var responseObj = JsonConvert.DeserializeObject<TResponse>(result);
            return responseObj;
        }

        private static HttpWebRequest CreateHttp(Uri uri, HttpMethod method)
        {
            HttpWebRequest http = WebRequest.CreateHttp(uri.AbsoluteUri);
            http.Method = method.Method;
            http.Accept = "application/json";
            http.UserAgent = ".Net App";
            return http;
        }

        private void SetCreditals(HttpWebRequest http)
        {
            string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                                           .GetBytes(username + ":" + password));
            http.Headers.Add("Authorization", "Basic " + encoded);
        }
        #endregion

    }
}
