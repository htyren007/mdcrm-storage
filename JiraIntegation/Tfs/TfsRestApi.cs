﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JiraIntegation.Tfs
{
    //public class TfsRestApi : IDisposable
    //{
    //    private string instance;
    //    private HttpClient client;
    //    private string username;
    //    private string password;

    //    public TfsRestApi(string instance)
    //    {
    //        this.instance = instance;
    //        client = new HttpClient();
    //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //    }

    //    public void Dispose()
    //    {
    //        client.Dispose();
    //    }

    //    #region Get
    //    public async Task<TfsResponse<TfsProject>> GetProjectsAsync()
    //    {
    //        Uri uri = new Uri($"https://{instance}/_apis/projects?api-version=3.2");
    //        var result = await GetResponseAsync<TfsResponse<TfsProject>>(uri);
    //        return result;

    //    }

    //    internal async Task<object> GetItems(string projectName)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{projectName}/_apis/tfvc/items?api-version=3.2");
    //        return await GetResponseAsync<object>(uri);
    //    }

    //    public async Task<TfsResponse<TfsBoard>> GetBoards(string projectName, string teamId)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{projectName}/{teamId}/_apis/work/boards?api-version=3.2");
    //        return await GetResponseAsync<TfsResponse<TfsBoard>>(uri);
    //    }

    //    public async Task<TfsBoard> GetBoard(string projectName, string teamId, string boardId)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{projectName}/{teamId}/_apis/work/boards/{boardId}?api-version=3.2");
    //        return await GetResponseAsync<TfsBoard>(uri);
    //    }

    //    internal async Task<TfsResponse<TfsItemType>> GetWorkItemTypes(string projectName)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{projectName}/_apis/wit/workitemtypes?ids=80557&api-version=3.2");
    //        return await GetResponseAsync<TfsResponse<TfsItemType>>(uri);
    //    }

    //    public async Task<TfsResponse<TfsTeam>> GetTeamsAsync(string id)
    //    {
    //        Uri uri = new Uri($"https://{instance}/_apis/projects/{id}/teams");
    //        return await GetResponseAsync<TfsResponse<TfsTeam>>(uri);
    //    }

    //    public void SetCredentials(string username, string password)
    //    {
    //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
    //                    Convert.ToBase64String(
    //                        System.Text.ASCIIEncoding.ASCII.GetBytes(
    //                            string.Format("{0}:{1}", username, password))));

    //        this.username = username;
    //        this.password = password;
    //    }

    //    internal async Task<TfsProject> GetProjectAsync(string projectId)
    //    {
    //        Uri uri = new Uri($"https://{instance}/_apis/projects/{projectId}?api-version=3.2");
    //        var result = await GetResponseAsync<TfsProject>(uri);
    //        return result;
    //    }

    //    public async Task<object> GetWorkItems(string project, string id)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{project}/_apis/wit/workitems/{id}?api-version=4.1");
    //        return await GetResponseAsync<object>(uri);
    //    }

    //    public async Task<object> GetBacklogs(string project, string team)
    //    {
    //        Uri uri = new Uri($"https://{instance}/{project}/_apis/work/backlogs?api-version=4.1");
    //        return await GetResponseAsync<object>(uri);
    //    }

    //    #endregion
    //    public async Task<object> PostWiql(WiqlRequest query)
    //    {
    //        Uri uri = new Uri($"https://{instance}/_apis/wit/wiql?api-version=3.2");
    //        return await PostResponseAsync<object>(uri, "Select [System.Id], [System.Title], [System.State] From WorkItems Where [System.WorkItemType] = 'Task'");
    //    }

    //    public async Task<object> GetBagsAsync(string project, string team)
    //    {
    //        //Uri uri = new Uri($"https://{instance}/{project}/_apis/wit/wiql?$top=50&api-version=3.2");
    //        Uri uri = new Uri($"https://{instance}/_apis/wit/wiql?$top=50&api-version=3.2");
    //        return await PostResponseAsync<object>(uri, "Select * From WorkItems Where [System.WorkItemType] = 'Task'");
    //    }

    //    internal async Task<TResponse> PostResponseAsync<TResponse>(Uri uri, string query)
    //    {
    //        try
    //        {
    //            var content = string.Format(@"{{""query"":""{0}""}}", query);
    //            HttpWebRequest http = WebRequest.CreateHttp(uri.AbsoluteUri);
    //            string encoded = System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
    //                           .GetBytes(username + ":" + password));
    //            http.Headers.Add("Authorization", "Basic " + encoded);

    //            byte[] postBytes = Encoding.UTF8.GetBytes(content);

    //            http.Method = HttpMethod.Post.Method;

    //            http.ContentType = "application/json; charset=UTF-8";
    //            http.Accept = "application/json";
    //            http.ContentLength = postBytes.Length;
    //            http.UserAgent = ".Net App";
    //            using (Stream requestStream = http.GetRequestStream())
    //            {
    //                requestStream.Write(postBytes, 0, postBytes.Length);
    //                requestStream.Close();
    //            }

    //            // now send it
    //            HttpWebResponse response = (HttpWebResponse)(await http.GetResponseAsync());
    //            string result;
    //            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
    //            {
    //                result = rdr.ReadToEnd();
    //            }

    //            var responseObj = JsonConvert.DeserializeObject<TResponse>(result);


    //            return responseObj;
    //        }
    //        catch (System.Net.Http.HttpRequestException exception)
    //        {
    //            throw;
    //        }
    //        catch (WebException webEx)
    //        {
    //            throw;
    //        }
    //    }

    //    internal async Task<TResponse> GetResponseAsync<TResponse>(Uri uri)
    //    {
    //        try
    //        {
    //            using (HttpResponseMessage response = await client.GetAsync(uri))
    //            {
    //                response.EnsureSuccessStatusCode();
    //                string responseStr = await response.Content.ReadAsStringAsync();
    //                var responseObj = JsonConvert.DeserializeObject<TResponse>(responseStr);
    //                return responseObj;
    //            }
    //        }
    //        catch (System.Net.Http.HttpRequestException exception)
    //        {
    //            return default;
    //        }
    //    }
    //}
}
