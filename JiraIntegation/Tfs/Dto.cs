﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace JiraTfsIntegation.Tfs.Dto
{
    public class WorkItemLink
    {
        public int id { get; set; }
        public string url { get; set; }
    }

    public class Column
    {
        public string referenceName { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }

    public class TfsWiqlResponse
    {
        public string queryType { get; set; }
        public string queryResultType { get; set; }
        public DateTime asOf { get; set; }
        public List<Column> columns { get; set; }
        public List<WorkItemLink> workItems { get; set; }
    }

    public class Fields: Href
    {
        [JsonProperty("System.AreaPath")]
        public string SystemAreaPath { get; set; }

        [JsonProperty("System.TeamProject")]
        public string SystemTeamProject { get; set; }

        [JsonProperty("System.IterationPath")]
        public string SystemIterationPath { get; set; }

        [JsonProperty("System.WorkItemType")]
        public string SystemWorkItemType { get; set; }

        [JsonProperty("System.State")]
        public string SystemState { get; set; }

        [JsonProperty("System.Reason")]
        public string SystemReason { get; set; }

        [JsonProperty("System.AssignedTo")]
        public string SystemAssignedTo { get; set; }

        [JsonProperty("System.CreatedDate")]
        public DateTime SystemCreatedDate { get; set; }

        [JsonProperty("System.CreatedBy")]
        public string SystemCreatedBy { get; set; }

        [JsonProperty("System.ChangedDate")]
        public DateTime SystemChangedDate { get; set; }

        [JsonProperty("System.ChangedBy")]
        public string SystemChangedBy { get; set; }

        [JsonProperty("System.Title")]
        public string SystemTitle { get; set; }

        [JsonProperty("Microsoft.VSTS.Common.StateChangeDate")]
        public DateTime MicrosoftVSTSCommonStateChangeDate { get; set; }

        [JsonProperty("Microsoft.VSTS.Common.Priority")]
        public int MicrosoftVSTSCommonPriority { get; set; }

        [JsonProperty("Microsoft.VSTS.Common.Activity")]
        public string MicrosoftVSTSCommonActivity { get; set; }

        [JsonProperty("GMCS.Priority")]
        public string GMCSPriority { get; set; }

        [JsonProperty("System.Description")]
        public string SystemDescription { get; set; }
    }

    public class Href
    {
        public string href { get; set; }
    }

    public class Links
    {
        public Href self { get; set; }
        public Href workItemUpdates { get; set; }
        public Href workItemRevisions { get; set; }
        public Href workItemHistory { get; set; }
        public Href html { get; set; }
        public Href workItemType { get; set; }
        public Fields fields { get; set; }
    }

    public class WorkItem
    {
        public int id { get; set; }
        public int rev { get; set; }
        public Fields fields { get; set; }
        public Links _links { get; set; }
        public string url { get; set; }
    }

}
