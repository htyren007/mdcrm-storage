﻿using JiraTfsIntegation.Jira;
using JiraTfsIntegation.Jira.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace JiraIntegation
{
    internal class JiraSimple
    {
        static string username = "ablokhin";
        static string password = "M4pC3H8wnTjh";

        internal static async Task Run()
        {
             await FindLastTasks();
            // await FindByKey();

            Console.ReadLine();
        }

        private static async Task FindByKey()
        {
            JiraRestClient jira = new JiraRestClient(@"https://bugz.yotateam.com");
            jira.AddCredentials(username, password);

            var result = await jira.GetByKey("CE-6223");
            Line("total: ", result.id);
            Line("expand: ", result.expand);
            Line("self: ", result.self);


            var id = "404096";
            result = await jira.GetById(id);
            Line("total: ", result.id);
            Line("expand: ", result.expand);
            Line("self: ", result.self);

        }

        private static async Task FindLastTasks()
        {
            JiraRestClient jira = new JiraRestClient(@"https://bugz.yotateam.com");
            jira.AddCredentials(username, password);

            var result = await jira.FindLastTasks("vkuzmina");
            //var result = await jira.FindLastTasks();

            Line("total: ", result.total);
            foreach (var issue in result.issues)
            {
                Line("   -----");
                Line("     id: ", issue.id);
                Line("     key: ", issue.key);
                Line("     created: ", issue.fields["created"]);
                var creator = issue.GetFields<Account>("creator");

                Line("     creator: ", creator.name);
            }
        }

        private static void Line(params object[] messages) => Console.WriteLine(string.Join(" ", messages));
        private static void Text(object message) => Console.Write(message.ToString());
        private static void Space() => Console.WriteLine();
    }
}