﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace JiraTfsIntegation.Jira.Dto
{
    public class JiraBase
    {
        public string id { get; set; }
        public string self { get; set; }
    }

    public class JiraBaseName : JiraBase
    {
        public string name { get; set; }
    }

    public class JiraFindResult
    {
        public string expand { get; set; }
        public int startAt { get; set; }
        public int maxResults { get; set; }
        public int total { get; set; }
        public List<Issue> issues { get; set; }
    }

    public class Issue : JiraBase
    {    
        public string key { get; set; }
        public string expand { get; set; }
        public Fields fields { get; set; }

        public TField GetFields<TField>(string key)
        {
            var jObject = (JObject)fields[key];
            return jObject.ToObject<TField>();
        }
    }

    

    public class Issuelink : JiraBase
    {
        public Type type { get; set; }
        public InwardIssue inwardIssue { get; set; }
        public OutwardIssue outwardIssue { get; set; }
    }

    public class InwardIssue : JiraBase
    {
        public string key { get; set; }
        public Fields fields { get; set; }
    }

    public class OutwardIssue : JiraBase
    {
        public string key { get; set; }
        public Fields fields { get; set; }
    }

    public class Fields : Dictionary<string, object>
    {
    }

    public class Resolution : JiraBaseName
    {
        public string description { get; set; }
    }

    public class Reporter
    {
        public string self { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string emailAddress { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
        public string timeZone { get; set; }
    }
    public class AvatarUrls
    {
        public string _48x48 { get; set; }
        public string _24x24 { get; set; }
        public string _16x16 { get; set; }
        public string _32x32 { get; set; }
    }
    public class Progress
    {
        public int progress { get; set; }
        public int total { get; set; }
        public int? percent { get; set; }
    }
    public class Aggregateprogress
    {
        public int progress { get; set; }
        public int total { get; set; }
        public int? percent { get; set; }
    }

    public class ProjectCategory : JiraBaseName
    {
        public string description { get; set; }
    }


    public class FixVersion : JiraBaseName
    {
        public string description { get; set; }
        public bool archived { get; set; }
        public bool released { get; set; }
        public string releaseDate { get; set; }
    }

    public class Watches
    {
        public string self { get; set; }
        public int watchCount { get; set; }
        public bool isWatching { get; set; }
    }

    public class Project : JiraBaseName
    {
        public string key { get; set; }
        public string projectTypeKey { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public ProjectCategory projectCategory { get; set; }
    }

    public class Child
    {
        public string self { get; set; }
        public string value { get; set; }
        public string id { get; set; }
        public bool disabled { get; set; }
    }


    public class Account
    {
        public string self { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string emailAddress { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
        public string timeZone { get; set; }
    }

    public class Type : JiraBaseName
    {
        public string inward { get; set; }
        public string outward { get; set; }
    }

    public class StatusCategory : JiraBaseName
    {
        public string key { get; set; }
        public string colorName { get; set; }
    }

    public class Status : JiraBaseName
    {
        public string description { get; set; }
        public string iconUrl { get; set; }
        public StatusCategory statusCategory { get; set; }
    }

    public class Priority : JiraBaseName
    {
        public string iconUrl { get; set; }
    }

    public class Issuetype : JiraBaseName
    {
        public string description { get; set; }
        public string iconUrl { get; set; }
        public bool subtask { get; set; }
        public int avatarId { get; set; }
    }
}
