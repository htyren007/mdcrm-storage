﻿using JiraIntegation.RestClient;
using JiraTfsIntegation.Jira.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JiraTfsIntegation.Jira
{
    public class JiraRestClient : ARestClient
    {
        private string username;
        private string password;

        public JiraRestClient(string basePath) : base(basePath)
        {
        }

        public void AddCredentials(string username, string password)
        {// Базовая авторизация
            this.username = username;
            this.password = password;
        }

        public async Task<JiraFindResult> FindLastTasks(string assignee = "",  int maxCount = 20)
        {
            if (string.IsNullOrEmpty(assignee))
                assignee = username;
            var jql = $"assignee={assignee}+order+by+created";
            var path = "rest/api/2/search?jql=" + jql+ "&fields=id,key,description,summary,created,creator,assignee,status";
            var result = await GetAsync<JiraFindResult>(path);
            return result;
        }

        public async Task<Issue> GetById(string id)
        {
            var path = $"rest/api/2/issue/{id}";
            return  await GetAsync<Issue>(path);
        }

        public async Task<Issue> GetByKey(string key)
        {
            var path = $"rest/api/2/issue/{key}?fields=description,summary,creator,assignee";
            var result = await GetAsync<Issue>(path);
            return result;
        }

        protected override void DefaultHeaders(HttpWebRequest http)
        {
            base.DefaultHeaders(http);

            string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1")
                               .GetBytes(username + ":" + password));

            http.Headers.Add("Authorization", $"Basic {encoded}");

            // http.Accept = "application/json";

        }
    }
}
