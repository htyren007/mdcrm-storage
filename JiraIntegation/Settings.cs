﻿using ConsoleStorage.INI;
using System;
using System.IO;

namespace JiraIntegation
{
    public class Settings
    {
        private const string SETTING_FILE_NAME = "settings.ini";

        public static void Load()
        {
            if (File.Exists(SETTING_FILE_NAME))
            {
                try
                {
                    LoadSettings();
                    return;
                }
                catch
                { 
                
                }
            }
            CreateSetting();
        }

        private static void LoadSettings()
        {
            var ini = IniData.LoadFile(SETTING_FILE_NAME);

            var main = ini.GetGroup("main");
            Integration.JIRA_URL = main.GetString("jira_url");
            Integration.TFS_URL = main.GetString("tfs_url");

            var users = ini.GetGroup("users");
            Integration.JIRA_USER = users.GetString("jira_login");
            Integration.JIRA_PASS = users.GetString("jira_pass");
            Integration.TFS_USER = users.GetString("tfs_login");
            Integration.TFS_PASS = users.GetString("tfs_pass");
            Integration.USER_ASSIGNEE = users.GetString("assignee");

            var createTask = ini.GetGroup("create_task");
            Integration.ITERATION_PATH = createTask.GetString("Sprint");
            Integration.PRIORITY = createTask.GetString("Priority");
        }

        private static void CreateSetting()
        {
            var ini = IniData.LoadFile(SETTING_FILE_NAME);
            var main = ini.GetGroup("main");
            main.Comment = new System.Collections.Generic.List<string>() {"Главные настройки программы"};

            main.SetString("jira_url", Integration.JIRA_URL, "Адрес сервера Jira");
            main.SetString("tfs_url", Integration.TFS_URL, "Адрес сервера Tfs");

            var users = ini.GetGroup("users");
            users.Comment = new System.Collections.Generic.List<string>() { "Настройки пользователей" };

            users.SetString("jira_login", Integration.JIRA_USER);
            users.SetString("jira_pass", Integration.JIRA_PASS);

            users.SetString("tfs_login", Integration.TFS_USER);
            users.SetString("tfs_pass", Integration.TFS_PASS);

            users.SetString("assignee", Integration.USER_ASSIGNEE,"Аккаунт по которому происходит поиск");

            var createTask = ini.GetGroup("create_task");
            createTask.Comment = new System.Collections.Generic.List<string>() { "Настройки создаваемой записи" };

            createTask.SetString("Sprint", Integration.ITERATION_PATH, "Спринт или доска на которой будет размещена новая задача");
            createTask.SetString("Priority", Integration.PRIORITY, "Приоритет новой задачи");


            ini.SaveFile();
        }
    }
}