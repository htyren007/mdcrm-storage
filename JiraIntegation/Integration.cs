﻿using JiraTfsIntegation.Jira;
using JiraTfsIntegation.Jira.Dto;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.WebApi.Patch;
using Microsoft.VisualStudio.Services.WebApi.Patch.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace JiraIntegation
{
    internal class Integration
    {
        public static string TEAM_PROJECT = "Yota";
        public static string JIRA_URL = @"https://bugz.yotateam.com";
        public static string TFS_URL = @"https://crm-tfs.gmcs.ru/tfs/CRM";

        public static string JIRA_USER = "ablokhin";
        public static string JIRA_PASS = "M4pC3H8wnTjh";
        public static string USER_ASSIGNEE = "vkuzmina";// assignee

        public static string TFS_USER = "GMCS\\rmiroshnikov";
        public static string TFS_PASS = "0h5t75";

        public static string ITERATION_PATH= "Yota\\ТП 1 кв.2022";
        public static string PRIORITY = "3. Средний";

        public static string[] VALID_STATUS = { "Open", "In Progress", "Reopened" };

        internal static async Task Run()
        {
            try
            {
                var jira = new JiraRestClient(JIRA_URL);
                jira.AddCredentials(JIRA_USER, JIRA_PASS);

                var tfs = new WorkItemTrackingHttpClient(
                            new Uri(TFS_URL),
                            new VssCredentials(new WindowsCredential(new NetworkCredential(TFS_USER, TFS_PASS))));
                Text("Подключение установлено!");
                var lastTasks = await jira.FindLastTasks(USER_ASSIGNEE);
                Line(" Получено ", lastTasks.issues.Count, "задач.");
                foreach (var issue in lastTasks.issues)
                {
                    Line("Задача", issue.key); //assignee


                    //var assignee = issue.GetFields<Account>("assignee");


                    if (!await ExistTfsAsync(tfs, issue))
                    {
                        CreateWorkItem(tfs, jira, issue);
                    }
                    else
                    {
                        Line("   - Задача пропущена");
                    }
                    Line("--------");
                }
            }
            catch (WebException webEx)// WebException
            {
                Line("\n--- Ошибка сети! --- ");
                Line(webEx.Status, ": ", webEx.Message);
            }
            catch (Exception ex)
            {
                Line("\n--- Внутренняя ошибка! --- ");
                Line(ex.GetType().Name, ": ", ex.Message);
            }

        }

        private static async void CreateWorkItem(WorkItemTrackingHttpClient tfs, JiraRestClient jira, Issue issue)
        {
            
            issue = await jira.GetById(issue.id);

            JsonPatchDocument document = new JsonPatchDocument();

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/Microsoft.VSTS.Common.Activity",
                Value = "Development"
            });

            var key = issue.key;
            var title = issue.fields["summary"];
            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.Title",
                Value = $"{key}: {title}",
            });

            var description = issue.fields["description"];
            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.Description",
                Value = description,
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.TeamProject",
                Value = TEAM_PROJECT
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/System.IterationPath",
                Value = ITERATION_PATH
            });

            document.Add(new JsonPatchOperation
            {
                From = null,
                Operation = Operation.Add,
                Path = "/fields/GMCS.Priority",
                Value = PRIORITY
            });

            var work = await tfs.CreateWorkItemAsync(document, TEAM_PROJECT, "Task");
            Line("Создана задача!");
            Line("id:", work.Id);
            Line("Title:", work.Fields["System.Title"]);
            Line("Description:", work.Fields["System.Description"]);
            Space();
            var type = work.Fields["System.WorkItemType"];
            Line("WorkItemType:", type);

            var teamProject = work.Fields["System.TeamProject"];
            Line("TeamProject:", teamProject);

            var path = work.Fields["System.IterationPath"];
            Line("IterationPath:", path);

            var priority = work.Fields["GMCS.Priority"];
            Line("Priority:", priority);

            var activity = work.Fields["Microsoft.VSTS.Common.Activity"];
            Line("Activity:", activity);
        }

        private static async Task<bool> ExistTfsAsync(WorkItemTrackingHttpClient tfs, JiraTfsIntegation.Jira.Dto.Issue issue)
        {
            var status = issue.GetFields<Status>("status");
            if (VALID_STATUS.Contains(status.statusCategory.name))
            {
                var result = await tfs.QueryByWiqlAsync(new Wiql()
                {
                    Query = @$"
Select [System.Id], [System.Title], [System.State] 
From WorkItems 
Where [System.Title] Contains '{issue.key}' 
order by [System.CreatedDate] desc"
                });

                if (result.WorkItems.Any())
                {
                    // var work = result.WorkItems.First();
                    return true;
                }
                return false;
            }
            return true;
        }


        private static void Line(params object[] messages) => Console.WriteLine(string.Join(" ", messages));
        private static void Text(object message) => Console.Write(message.ToString());
        private static void Space() => Console.WriteLine();
    }
}