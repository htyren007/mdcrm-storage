﻿using JiraTfsIntegation.Jira.Dto;
using System;
using System.Collections.Generic;

namespace JiraIntegation
{
    public class IssueAdapter
    {
        public int? aggregatetimeestimate { get; set; }
        public int? aggregatetimespent { get; set; }
        public int? timeestimate { get; set; }
        public int? timespent { get; set; }
        public int workratio { get; set; }
        public string summary { get; set; }
        public string description { get; set; }
        public DateTime? lastViewed { get; set; }
        public DateTime? customfield_10042 { get; set; }
        public DateTime resolutiondate { get; set; }
        public DateTime updated { get; set; }
        public DateTime created { get; set; }
        public Aggregateprogress aggregateprogress { get; set; }
        public Account assignee { get; set; }
        public Status status { get; set; }
        public Priority priority { get; set; }
        public Issuetype issuetype { get; set; }
        public Resolution resolution { get; set; }
        public Account creator { get; set; }
        public Reporter reporter { get; set; }
        public Progress progress { get; set; }
        public Project project { get; set; }
        public Watches watches { get; set; }
        public List<Issuelink> issuelinks { get; set; }
        public List<object> subtasks { get; set; }
        public List<object> components { get; set; }
        public List<object> labels { get; set; }
        public List<FixVersion> fixVersions { get; set; }
        public List<object> versions { get; set; }
    }
}