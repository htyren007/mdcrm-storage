﻿using System.Threading.Tasks;

namespace JiraIntegation
{
    internal class Program
    {


        static async Task Main(string[] args)
        {
            Settings.Load();
            // await WorkItemTrackingSimple.Run();
            //await JiraSimple.Run();
            
            await Integration.Run();
        }
    }
}
