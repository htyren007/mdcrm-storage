﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JiraIntegation.RestClient
{
    public abstract class ARestClient 
    {
        private string basePath;

        public ARestClient(string basePath)
        {
            this.basePath = basePath;
        }

        public async Task<TResponse> GetAsync<TResponse>(string relativePath)
        {
            UriBuilder uri = new UriBuilder(basePath+"/"+relativePath);
            HttpWebRequest http = CreateHttp(uri.Uri, HttpMethod.Get);
            return await GetResponseAndJsonDeserialize<TResponse>(http);
        }

        protected virtual void DefaultHeaders(HttpWebRequest http)
        {
            http.UserAgent = ".Net App";
        }

        private HttpWebRequest CreateHttp(Uri uri, HttpMethod method)
        {
            HttpWebRequest http = WebRequest.CreateHttp(uri.AbsoluteUri);
            http.Method = method.Method;
            DefaultHeaders(http);
            return http;
        }

        private static async Task<TResponse> GetResponseAndJsonDeserialize<TResponse>(HttpWebRequest http)
        {
            HttpWebResponse response = (HttpWebResponse)(await http.GetResponseAsync());
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            var responseObj = JsonConvert.DeserializeObject<TResponse>(result);
            return responseObj;
        }
    }
}
