﻿using ConsoleStorage.INI;
using hty.Rest.Yota;
using System;
using System.Diagnostics;

namespace ConsoleRestClient
{
    // http://swagger.g4lab.com/?service=PES&version=10.25.x
    // http://swagger.g4lab.com/?service=PMSv2&version=10.25.x
    // http://swagger.g4lab.com/?service=DMS&version=10.12.x
    //
    public class Settings : SettingsBase
    {
        private const string SETTING_FILENAME = "data.ini";
        private static Settings _instance;


        public Settings(string filename) : base(filename)
        {
            _General = new GeneralSettingsGroup(file);
            _Paths = new PathsSettingsGroup(file);
        }

        public static Settings Instance
        {
            get 
            {
                if (_instance == null)
                    Load();
                return _instance;
            }
        }

        public static GeneralSettingsGroup General => _instance._General;
        public static PathsSettingsGroup Paths => _instance._Paths;

        public GeneralSettingsGroup _General;
        public PathsSettingsGroup _Paths;

        internal static void SetDefault()
        {
            General.SetDefault();
            Paths.SetDefault();
        }

        public static void Load(bool force = true)
        {
            _instance = new Settings(SETTING_FILENAME);
            if (force)
            {
                SetDefault();
                Save();
            }
        }

        public static void Save() => _instance.SaveSettings();

        internal static void Open()
        {
            Process.Start("notepad.exe", SETTING_FILENAME);
        }
    }

    public class PathsSettingsGroup : SettingsGroup, IYotaSettings
    {
        private const string GROUPNAME = "paths";

        public PathsSettingsGroup(IniData file) : base(file, GROUPNAME)
        {
        }
        public string Pes { get => GetStringSetting(); set => SetStringSetting(value); }
        public string Dms { get => GetStringSetting(); set => SetStringSetting(value); }
        public string Rids { get => GetStringSetting(); set => SetStringSetting(value); }
        public string CisRest { get => GetStringSetting(); set => SetStringSetting(value); }
        public string Cids { get => GetStringSetting(); set => SetStringSetting(value); }
        public string FT3 { get => GetStringSetting(); set => SetStringSetting(value); }

        protected override void CreateDefault()
        {
            base.CreateDefault();
            group.SetString(nameof(Pes), "http://ft2-pm-api.g4lab.com:8080/pm/v1");
            group.SetString(nameof(Dms), "http://ft2-pm-api.g4lab.com:8080/pm/v1");
            group.SetString(nameof(Rids), "http://ft2-ec-api.g4lab.com:8080/ec/v1");
            group.SetString(nameof(CisRest), "http://ft2-cd-api.g4lab.com:8080/cdm/v1");
            group.SetString(nameof(Cids), "http://ft2-cd-api.g4lab.com:8080/cdm/v1");
            group.SetString(nameof(FT3), "http://ft3-pm-api.g4lab.com:8080");
        }

        internal void SetDefault() => CreateDefault();
    }

    public class GeneralSettingsGroup : SettingsGroup
    {
        private const string GENERAL_GROUPNAME = "general";

        public GeneralSettingsGroup(IniData file) : base(file, GENERAL_GROUPNAME) { }

        // public string BasePath { get => GetStringSetting(); set => SetStringSetting(value); }
        public string CustomerId { get => GetStringSetting(); set => SetStringSetting(value); }
        public string AccountId { get => GetStringSetting(); set => SetStringSetting(value); }
        public string CustomersFolder { get => GetStringSetting(); set => SetStringSetting(value); }

        protected override void CreateDefault()
        {
            base.CreateDefault();
            group.Clear();
            // group.SetString(nameof(BasePath), "http://ft2-pm-api.g4lab.com:8080");
            //group.SetString(nameof(CustomerId), "965c6dca76114dd3b2c2c1dec4561b50");
            //group.SetString(nameof(AccountId), "11220029982312");// 11220029940974
            group.SetString(nameof(CustomersFolder), @"D:\hty\temp\Customers\");
        }

        internal void SetDefault() => CreateDefault();
    }
}