﻿using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using ConsoleStorage.Utility;
using System;
using System.Collections.Generic;

namespace ConsoleRestClient.Menu
{
    [SlideTitle(null)]
    public class CustomerList
    {
        public bool NeedReopen { get; private set; } = false;

        [SlideSetup]
        public void Setup(CustomForm slide)
        {
            var header = "=== К л и е н т ы ===";
            slide.Add(new ColorLabel()
            {
                Text = header,
                Left = Console.WindowWidth / 2 - header.Length / 2,
                Top = 1,
                Background = ConsoleColor.Gray,
                Foreground = ConsoleColor.Black
            });
            int left = 5;
            int top = 3;
            slide.Add(new Label("-------------------------------------------------------", 0, top++));

            foreach (var customer in Control.Customers)
            {
                var button = new Button();
                button.Content = customer.Id;
                button.Left = left;
                button.Top = top++;
                button.Action = () => ShowCustomer(customer, slide, button);

                slide.Add(button);
                if (top > 30)
                    break;
            }
        }

        private void ShowCustomer(Dto.CustomerInfo customer, CustomForm slide, Button button)
        {
            var option = new Options<CustomerDetals>();
            option.Controller.Model = customer;
            
            option.Run();
            if (option.Controller.IsDeleted)
            {
                NeedReopen = true;
                slide.NextSlide = null;
            }
        }

        [SlideButton(dispayName: "<- Назад", left: 2, top: 1)]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }

        [SlideButton(dispayName: "Добавить", left: 4, top: 2)]
        public void Add(CustomForm slide)
        {
            var id = ConsoleHelper.Query("Введите guid:");
            if (!string.IsNullOrWhiteSpace(id))
            {
                Dto.CustomerInfo customer = new Dto.CustomerInfo() { Id = id };
                var name = ConsoleHelper.Query("Введите имя:");
                customer.Name = string.IsNullOrWhiteSpace(name) ? null : name;
                Control.Customers.Save(customer);
                NeedReopen = true;
                slide.NextSlide = null;
            }
        }
    }
}