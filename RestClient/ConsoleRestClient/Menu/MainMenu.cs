﻿using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.Utility;

namespace ConsoleRestClient.Menu
{
    [SlideTitle("Rest client")]
    public class MainMenu 
    {
        public MainMenu()
        {
            Settings.Load();
            Control.Initialize();
        }

        [SlideButton(dispayName: "Клиенты")]
        public void Clients()
        {
            while(true)
            {
                var option = new Options<CustomerList>();
                option.Run();

                if (!option.Controller.NeedReopen)
                    break;
            }
        }

        [SlideButton(dispayName: "Сервисы")]
        public void Servises()
        {
            var option = new Options<ServiseList>();
            option.Run();
        }

        [SlideButton(dispayName: "Настройки")]
        public void OpenSettings()
        {
            var option = new Options<SettingsForm>();
            option.Run();
        }

        [SlideButton(dispayName: "Выход")]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }
    }
}