﻿using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using System;

namespace ConsoleRestClient.Menu
{
    [SlideTitle(null)]
    internal class ServiseList
    {
        [SlideSetup]
        public void Setup(CustomForm slide)
        {
            var header = " === С е р в и с ы === ";
            slide.Add(new ColorLabel()
            {
                Text = header,
                Left = Console.WindowWidth / 2 - header.Length/2,
                Top = 0,
                Background = ConsoleColor.Gray,
                Foreground = ConsoleColor.Black
            });
            int left = 5;
            int top = 2;
            slide.Add(new Label("-------------------------------------------------------", 0, top++));

        }

        [SlideButton(dispayName: "<- Назад", left: 2, top: 1)]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }
    }
}