﻿using ConsoleRestClient.Dto;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using ConsoleStorage.Utility;
using hty.Rest.Yota;
using System;

namespace ConsoleRestClient.Menu
{
    [SlideTitle(null)]
    internal class CustomerDetals
    {
        public CustomerInfo Model { get; internal set; }
        public ResponseCollection Responses { get; internal set; } = new ResponseCollection();

        public bool IsDeleted { get; private set; } = false;

        [SlideSetup]
        public void Setup(CustomForm slide)
        {
            var header = "=== Д е т а л и ===";
            int left = 5;
            int top = 3;
            slide.Add(new ColorLabel()
            {
                Text = header,
                Left = Console.WindowWidth / 2 - header.Length / 2,
                Top = 1,
                Background = ConsoleColor.Gray,
                Foreground = ConsoleColor.Black
            });
            slide.Add(new Label($"id: {Model.Id}", left, top++));
            slide.Add(new Label($"Msisdn: {Model.Msisdn}", left, top++));
            slide.Add(new Label($"Name: {Model.Name}", left, top++));

            var button = new Button();
            button.Content = "Удалить";
            button.Left = Console.WindowWidth - 10;
            button.Top = 3;
            button.Action = () => Delete(slide);
            slide.Add(button);
        }

        private void Delete(CustomForm slide)
        {
            Console.Clear();
            var key = ConsoleHelper.QueryKey("Вы действительно хотите удалить запись? [y/n]: ");
            if (key.Key == ConsoleKey.Y || key.Key == ConsoleKey.L)
            {
                Control.Customers.Delete(Model);
                IsDeleted = true;
                slide.NextSlide = null;
            }
        }

        [SlideButton(dispayName: "<- Назад", left: 2, top: 1)]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }

        [SlideButton(dispayName: "PersonalData", left: 60, top: 6)]
        public void CustomerPd(ASlide slide)
        {
            YotaClient client = new YotaClient(Settings.Paths);
            if (string.IsNullOrWhiteSpace(Model.Msisdn))
            {
                var getter0 = client.RIDS.Identifiers()
                     .KnownId(Model.Id)
                     .KnownIdType(IdType.customerId);

                if (getter0.Execute())
                {
                    Responses.Identifiers = getter0.Result;
                    Model.Msisdn = Responses.Identifiers.msisdn;
                }
                else
                {
                    ConsoleHelper.Query("Ошибка!");
                }
            }

            var getter = client.CISRest.CustomerPd().Msisdn(Model.Msisdn);

            if (getter.Execute())
            {
                Responses.PersonalData = getter.Result;
                ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.firstName), Responses.PersonalData.name.firstName);
                ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.lastName), Responses.PersonalData.name.lastName);
                ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.middleName), Responses.PersonalData.name.middleName);
                Model.Name = Responses.PersonalData.name.firstName + " " + Responses.PersonalData.name.lastName;
                Control.Customers.Save(Model);
                ConsoleHelper.Query("Выполнено!");
            }
            else
            {
                ConsoleHelper.Query("Ошибка!");
            }
            // Console.Write(json);


        }

        [SlideButton(dispayName: "Identifiers", left: 60, top: 7)]
        public void Identifiers(ASlide slide)
        {
            if (Responses.Identifiers == null)
            {
                YotaClient client = new YotaClient(Settings.Paths);
                // Responses.Identifiers = client.RIDS.Identifiers(Model.Id, IdType.customerId);
                var getter = client.RIDS.Identifiers()
                    .KnownId(Model.Id)
                    .KnownIdType(IdType.customerId);

                if (getter.Execute())
                {
                    Responses.Identifiers = getter.Result;
                    Model.Msisdn = Responses.Identifiers.msisdn;

                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.accountId), Responses.Identifiers.accountId);
                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.customerId), Responses.Identifiers.customerId);
                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.iccid), Responses.Identifiers.iccid);
                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.imsi), Responses.Identifiers.imsi);
                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.msisdn), Responses.Identifiers.msisdn);
                    ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.simExternalId), Responses.Identifiers.simExternalId);

                }
                else
                { }
            }
            //ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.), Responses.Identifiers.simExternalId);


            ConsoleHelper.Query("");
        }

        //[SlideButton(dispayName: "Customer", left: 60, top: 8)]
        //public void Customer(ASlide slide)
        //{
        //    YotaClient client = new YotaClient(Settings.Paths);
        //    if (Responses.CisCustumer == null)
        //        Responses.CisCustumer = client.CISRest.Customer(Model.Id, IdType.customerId);

        //    //ConsoleHelper.WriteProperty(nameof(Responses.Identifiers.), Responses.Identifiers.simExternalId);

        //    ConsoleHelper.Query("Выполнено");
        //}

        //[SlideButton(dispayName: "AccountDetails", left: 60, top: 9)]
        //public void AccountDetails(ASlide slide)
        //{
        //    YotaClient client = new YotaClient(Settings.Paths);
        //    if (Responses.PersonalData == null)
        //        Responses.PersonalData = client.CISRest.GetAccountDetails(Model.Id, IdType.customerId);
        //    //var json = client.CISRest.GetAccountDetails(Model.Id, IdType.customerId);

        //    //Console.Write(json);
        //    ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.firstName), Responses.PersonalData.name.firstName);
        //    ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.lastName), Responses.PersonalData.name.lastName);
        //    ConsoleHelper.WriteProperty(nameof(Responses.PersonalData.name.middleName), Responses.PersonalData.name.middleName);

        //    //Name = Responses.AccountDetails.name.firstName + " " + Responses.AccountDetails.name.lastName;

        //    ConsoleHelper.Query("Выполнено!");
        //}


    }


}