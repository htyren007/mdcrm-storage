﻿using hty.Rest.Yota.Dto;
using hty.Rest.Yota.Dto.Rids;
using System;
using YotaRest.Services.CisRest;

namespace ConsoleRestClient.Dto
{
    [Serializable]
    public class CustomerInfo
    {
        //[NonSerialized]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Msisdn { get; internal set; }
    }

    [Serializable]
    public class ResponseCollection
    {
        public IdentifiersResponse Identifiers { get; set; }
        public PersonalDataResponse PersonalData { get; internal set; }
        public CisCustomer CisCustumer { get; internal set; }
    }
}
