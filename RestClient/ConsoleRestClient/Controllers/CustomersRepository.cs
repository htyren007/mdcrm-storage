﻿using ConsoleRestClient.Dto;
using hty.Rest.Yota;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace ConsoleRestClient
{
    public class CustomersRepository : IEnumerable<CustomerInfo>
    {
        private List<CustomerInfo> data;
        private DirectoryInfo dir;

        public CustomersRepository(string nameDir)
        {
            this.dir = new DirectoryInfo (nameDir);
            dir.Create();
            var filename = GetFileName("list");
            if (File.Exists(filename))
            {
                var json = File.ReadAllText(filename);
                data = JsonSerializer.Deserialize<List<CustomerInfo>>(json);
            }
            else
            {
                data = new List<CustomerInfo>();
            }
        }

        /// <summary>Существует запись</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool IsExist(string id) => File.Exists(GetFileName(id));

        private string GetFileName(string id) => Path.Combine(dir.FullName, $"{id}.json");


        public void Save(CustomerInfo customerInfo)
        {
            var index = data.FindIndex(x => x.Id == customerInfo.Id);
            if (index < 0)
                data.Add(customerInfo);
            else
                data[index] = customerInfo;

            WriteData();
        }

        public void Delete(CustomerInfo info)
        {
            var index = data.FindIndex(x => x.Id == info.Id);
            data.RemoveAt(index);
            WriteData();
        }

        public IEnumerator<CustomerInfo> GetEnumerator()
        {
            return ((IEnumerable<CustomerInfo>)data).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)data).GetEnumerator();
        }

        private void WriteData()
        {
            var json = JsonSerializer.Serialize(data, new JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText(GetFileName("list"), json);
        }
    }
}