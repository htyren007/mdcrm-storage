﻿using hty.Rest.Yota;
using System;

namespace ConsoleRestClient
{

    public static class Control
    {
        // private static CustomersController customersController;
        //private static YotaClient yotaClient;
        private static CustomersRepository customersRepository;
        //private static ServiceController serviceControl;

        // public static CustomersController Customers => customersController;
        //public static YotaClient YotaClient => yotaClient;
        public static CustomersRepository Customers => customersRepository;

        //public static ServiceController ServiceControl => serviceControl;

        internal static void Initialize(/*PathsSettingsGroup paths*/)
        {
            //try
            //{
            //    //yotaClient = new YotaClient(paths);
            //}
            //catch
            //{
            //    Settings.SetDefault();
            //    //yotaClient = new YotaClient(paths);

            //}

            customersRepository = new CustomersRepository(Settings.General.CustomersFolder);
            //customersController = new CustomersController();
            // serviceControl = new ServiceController();
        }
    }

    
}