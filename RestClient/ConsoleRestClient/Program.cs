﻿using ConsoleRestClient.Menu;
using ConsoleStorage.Command;
using ConsoleStorage.ConsoleMenu;

namespace ConsoleRestClient
{
    public class Program
    {
        internal static Executor exe;

        public static void Main(string[] args)
        {
            var menu = new Menu<MainMenu>();
            menu.Run();
        }
    }
}
