﻿using hty.Rest.Yota.Pes;
using hty.Rest.Yota.Pm;

namespace hty.Rest.Yota
{
    public class YotaClient
    {


        public YotaClient(IYotaSettings settings)
        {
            PES = new PesClient(settings.Pes);
            PM = new PmClient(settings.Pm);
            DMS = new DmsClient(settings.Dms);
            RIDS = new RidsClient(settings.Rids);
            CISRest = new CisRestClient(settings.CisRest);
            CIDS = new CidsClient(settings.Cids);
        }

        public PesClient PES { get; internal set; }
        public PmClient PM { get; internal set; }
        public DmsClient DMS { get; internal set; }
        public RidsClient RIDS { get; internal set; }
        public CisRestClient CISRest { get; internal set; }
        public CidsClient CIDS { get; internal set; }
    }
}