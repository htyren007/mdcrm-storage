﻿namespace hty.Rest.Yota
{
    public interface IYotaSettings
    {
        string Pes { get; set; }
        string Dms { get; set; }
        string Rids { get; set; }
        string CisRest { get; set; }
        string Cids { get; set; }
        string Pm { get; set; }
    }
}