﻿using System;

namespace hty.Rest.Yota.Pes
{

    public class PesClient : ARestClient
    {
        public PesClient(string basePath) : base(basePath) { }

        public SimcardsGetter GetSimcards(string id)
        {
            return new SimcardsGetter(BasePath).CustomerId(id);
            // return Get<List<SimcardsResponse>>($"simcards?customerId={id}&returnProducts=true&returnDiscounts=true");
        }
    }
}