﻿using System.Collections.Generic;
using System.Text;
using YotaRest.Services;

namespace hty.Rest.Yota.Pes
{
    public class SimcardsGetter : BaseGetter<List<SimcardsResponse>>
    {
        private const string relativePath = "simcards";
        private string customerId;
        private bool? returnProducts;
        private bool? returnDiscounts;

        public SimcardsGetter(string basePath) : base(basePath, relativePath)
        {
        }

        public SimcardsGetter CustomerId(string value)
        {
            request.QueryParam("customerId", value);
            return this;
        }
        public SimcardsGetter ReturnProducts(bool value)
        {
            request.QueryParam("returnProducts", value.ToString());
            return this;
        }

        public SimcardsGetter ReturnDiscounts(bool value)
        {
            request.QueryParam("returnDiscounts", value.ToString());
            return this;
        }

        //protected override void CreateUri()
        //{
        //    StringBuilder builder = new StringBuilder($"customerId={customerId}");
        //    if (returnProducts.HasValue)
        //    {
        //        builder.Append("&returnProducts=");
        //        builder.Append(returnProducts.Value.ToString());
        //    }
        //    if (returnDiscounts.HasValue)
        //    {
        //        builder.Append("&returnDiscounts=");
        //        builder.Append(returnDiscounts.Value.ToString());
        //    }
        //    uri.Query = builder.ToString();
        //}
    }
}