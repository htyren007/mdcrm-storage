﻿using hty.Rest.Yota.Dto;
using System;
using System.Collections.Generic;

namespace hty.Rest.Yota.Pes
{
    [Serializable]
    public class SimcardsResponse
    {
        public DateTime activated { get; set; }
        public string productLine { get; set; }
        public int accountId { get; set; }
        public string baseProductRegion { get; set; }
        public string homeRegion { get; set; }
        public int externalId { get; set; }
        public string imsi { get; set; }
        public string iccid { get; set; }
        public string msisdn { get; set; }
        public string simType { get; set; }
        public string accountType { get; set; }
        public string simFormFactor { get; set; }
        public string simSpec { get; set; }
        public Status status { get; set; }
        public List<Product> products { get; set; }
        public List<Discount> discounts { get; set; }
    }
}
