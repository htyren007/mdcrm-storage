﻿using hty.Rest.Yota.Dto.Rids;

namespace hty.Rest.Yota
{
    public class RidsClient : ARestClient
    {
        public RidsClient(string basePath) : base(basePath) { }

        public IdentifiersGetter Identifiers()
        {
            return new IdentifiersGetter(BasePath);
        }
    }
}
