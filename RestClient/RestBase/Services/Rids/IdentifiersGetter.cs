﻿using hty.Rest.Yota.Dto.Rids;
using System;
using YotaRest.Services;

namespace hty.Rest.Yota
{
    public class IdentifiersGetter : BaseGetter<IdentifiersResponse>
    {
        public IdentifiersGetter(string basePath) : base(basePath, "identifiers")
        {
        }

        public IdentifiersGetter KnownId(string id)
        {
            request.QueryParam("knownId", id);
            return this;
        }

        public IdentifiersGetter KnownIdType(IdType customerId)
        {
            request.QueryParam("knownIdType", customerId.ToString()); 
            return this;
        }
    }
}