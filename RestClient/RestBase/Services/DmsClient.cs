﻿using hty.Rest.Yota.Dto.Dms;
using System;

namespace hty.Rest.Yota
{
    public class DmsClient : ARestClient
    {
        public DmsClient(string basePath) : base(basePath){}

        public bool DeleteDiscount(string customerId, string discountId)
        {
            // http://swagger.g4lab.com/?service=DMS&version=10.12.x
            try
            {
                var response = PostString("pm/v1/discounts/close", $@"{{
""customerId"": ""{customerId}"",
""salesChannel"": ""CRM"",
""discountId"": {discountId}
}}");
                Console.WriteLine(response);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public GetDiscountsAvaibleResponse GetDiscountsAvailable(string customerId, bool returnCurrentProducts, bool returnCurrentDiscounts)
        {
            var response = Get<GetDiscountsAvaibleResponse>($"pm/v1/discounts/available?customerId={customerId}&salesChannel=CRM&returnCurrentProducts={returnCurrentProducts}&returnCurrentDiscounts={returnCurrentDiscounts}");
            return response;
        }
    }

}