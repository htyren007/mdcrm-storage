﻿using hty.Rest.Yota.Dto;
using System;

namespace YotaRest.Services.CisRest
{
    [Serializable]
    public class PersonalDataResponse
    {
        public Address address { get; set; }
        public Document document { get; set; }
        public Name name { get; set; }
        public PersonalDetails personalDetails { get; set; }
        public int accountId { get; set; }
        public string type { get; set; }
        public DateTime actualityDate { get; set; }
        public string typeCheck { get; set; }
    }
}
