﻿namespace YotaRest.Services.CisRest
{
    public class PersonalDataGetter: BaseGetter<PersonalDataResponse>
    {
        private const string relativePath = "customer/pd";

        public PersonalDataGetter(string basePath) : base(basePath, relativePath)
        {
        }

        public PersonalDataGetter Msisdn(string msisdn)
        {
            request.QueryParam("msisdn",msisdn);
            return this;
        }
    }
}
