﻿namespace YotaRest.Services
{
    public interface IRestGetter
    {
        bool Execute();
    }
}