﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using YotaRest.Requests;

namespace YotaRest.Services
{
    public class BaseGetter<TResponse>
    {
        protected UniversalRestRequest request;
        //protected UriBuilder uri;
        //protected HttpWebRequest http;
        //protected HttpMethod method = HttpMethod.Get;
        //protected HttpWebResponse response;
        private string textResponse;

        public TResponse Result { get; set; }

        public BaseGetter(string basePath, string relativePath)
        {
            request = new UniversalRestRequest(basePath, relativePath);
            //this.basePath = basePath;
            // this.relativePath = relativePath;
            //uri = new UriBuilder(basePath + "/" + relativePath);
        }

        public Exception Error => request?.Error;
        public bool IsError => request?.IsError ?? false;
        public string TextResponse => request?.Response;

        public bool Execute()
        {
            request.Send();
            if (!request.IsError)
            {
                Result = JsonConvert.DeserializeObject<TResponse>(TextResponse);
            }

            return !request.IsError;
            //CreateUri();
            //http = CreateHttp(uri.Uri, method);
            //try
            //{
            //    response = (HttpWebResponse)http.GetResponse();
            //    using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            //    {
            //        TextResponse = rdr.ReadToEnd();
            //    }
            //    Result = JsonConvert.DeserializeObject<TResponse>(TextResponse);
            //    return !(IsError = false);
            //}
            //catch (Exception ex) 
            //{
            //    Error = ex;
            //}
            //return !(IsError = true);
        }

        protected virtual void CreateUri()
        {
        }

        protected virtual void DefaultHeaders(HttpWebRequest http)
        {
            http.UserAgent = ".Net App";
        }

        private HttpWebRequest CreateHttp(Uri uri, HttpMethod method)
        {
            HttpWebRequest http = WebRequest.CreateHttp(uri.AbsoluteUri);
            http.Method = method.Method;
            DefaultHeaders(http);
            return http;
        }
    }
}