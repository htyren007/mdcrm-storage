﻿using hty.Rest.Yota.Dto;
using hty.Rest.Yota.Dto.Cis;
using hty.Rest.Yota.Dto.Rids;
using YotaRest.Services.CisRest;

namespace hty.Rest.Yota
{
    public class CisRestClient : ARestClient
    {
        // http://swagger.g4lab.com/?service=CIS
        // http://swagger.g4lab.com/?service=CIS&version=9.40.x
        // connectionString="http://ft3-cd-api.g4lab.com:8080/cdm/v1"
        public CisRestClient(string basePath) : base(basePath) { }

        public PersonalDataResponse GetAccountDetails(string id, IdType type,string date = null)
        {
            //Требует заполнения одного и только одного из входных параметров: customerId, accountId, msisdn

            string typeId = type == IdType.customerId ? "customerId"
                : type == IdType.accountId ? "accountId"
                : type == IdType.msisdn ? "msisdn"
                : throw new System.Exception("Требует заполнения одного и только одного из входных параметров: customerId, accountId, msisdn");
            
            var query = $"customer/account/details?{typeId}={id}";
                var response = Get<PersonalDataResponse>(query);
                return response;
        }

        public PersonalDataGetter CustomerPd() 
        { //  customer/pd   Получить персональные данные абонента
            return new PersonalDataGetter(BasePath);
            //string query = $"customer/pd?msisdn={msisdn}";
            //var response = Get<PersonalDataResponse>(query);
            //return response;
        }

        public CisCustomer Customer(string id, IdType type)
        {
            string typeId = type == IdType.customerId ? "customerId"
                : type == IdType.accountId ? "accountId"
                : type == IdType.msisdn ? "msisdn"
                : type == IdType.iccid ? "iccid"
                : "accountId";

            string query = $"customer?{typeId}={id}";
            var response = Get<CisCustomer>(query);
            return response;
        }
    }

    // 

    

}
