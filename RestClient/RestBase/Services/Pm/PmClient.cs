﻿namespace hty.Rest.Yota.Pm
{
    public class PmClient : ARestClient
    {
        public PmClient(string basePath) : base(basePath)
        {
        }
        public ProductsAvailableGetter ProductsAvailable()
        {
            return new ProductsAvailableGetter(BasePath);
        }
    }
}