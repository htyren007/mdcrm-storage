﻿using hty.Rest.Yota.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace hty.Rest.Yota.Pm
{
    [Serializable]
    public class GetAvailableProductsResponse
    {
        [JsonProperty("products")]
        public List<AvailableProduct> Products { get; set; }

        [JsonProperty("options")]
        public List<AvailableProductOption> Options { get; set; }

        [JsonProperty("currentProducts")]
        public List<Product> CurrentProducts { get; set; }
    }
}