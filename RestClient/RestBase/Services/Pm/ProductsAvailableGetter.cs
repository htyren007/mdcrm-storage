﻿using System.Text;
using YotaRest.Services;

namespace hty.Rest.Yota.Pm
{
    public class ProductsAvailableGetter : BaseGetter<GetAvailableProductsResponse>
    {
        private const string relativePath = "products/available";
        private string customerId;
        private uint? accountId;
        private string imsi;
        private AccauntTypeEnum accauntType = AccauntTypeEnum.B2C;
        private string salesChannel = "CRM";
        private ProductTypeFilter filter = ProductTypeFilter.allin;
        private bool? returnCurrentProducts;
        private CustomerRoleEnum? customerRole;

        public ProductsAvailableGetter(string basePath) : base(basePath, relativePath)
        {
        }

        protected override void CreateUri()
        {
            //StringBuilder builder = new StringBuilder(); 
            //if (!string.IsNullOrWhiteSpace(customerId))
            //{
            //    builder.Append($"customerId={customerId}");
            //}
            //else if (!string.IsNullOrWhiteSpace(imsi))
            //{
            //    builder.Append($"imsi={imsi}");
            //}
            //else if (accountId.HasValue)
            //{
            //    builder.Append($"accountId={accountId.Value}");
            //}

            //builder.Append("&accType=");
            //builder.Append(accauntType);

            //builder.Append("&filter=");
            //builder.Append(filter);

            //builder.Append("&salesChannel=");
            //builder.Append(salesChannel);

            //if (returnCurrentProducts.HasValue)
            //{
            //    builder.Append("&returnCurrentProducts=");
            //    builder.Append(returnCurrentProducts.Value);
            //}

            //if (customerRole.HasValue)
            //{
            //    builder.Append("&customerRole=");
            //    builder.Append(customerRole.Value);
            //}

            //uri.Query = builder.ToString();
        }

        public ProductsAvailableGetter AccauntType(AccauntTypeEnum value)
        {
            accauntType = value;
            return this;
        }

        public ProductsAvailableGetter Filter(ProductTypeFilter value)
        {
            this.filter = value;
            return this;
        }

        public ProductsAvailableGetter ReturnCurrentProducts(bool value)
        {
            this.returnCurrentProducts = value;
            return this;
        }

        public ProductsAvailableGetter CustomerRole(CustomerRoleEnum value)
        {
            this.customerRole = value;
            return this;
        }

        public ProductsAvailableGetter SalesChannel(string value)
        {
            salesChannel = value;
            return this;
        }

        public ProductsAvailableGetter CustomerId(string id)
        {
            customerId = id;
            accountId = null;
            imsi = null;
            return this;
        }

        public ProductsAvailableGetter AccountId(uint id)
        {
            customerId = null;
            accountId = id;
            imsi = null;
            return this;
        }

        public ProductsAvailableGetter Imsi(string value)
        {
            customerId = null;
            accountId = null;
            imsi = value;
            return this;
        }

    }
    public enum CustomerRoleEnum
    {
        ACCEPTOR, ALONE, DONOR, MASTER
    }

    public enum ProductTypeFilter
    {
        packages, packageOptions, baseProductOptions, misc, vas, allin
    }
    public enum AccauntTypeEnum
    {
        B2C, B2B
    }
}