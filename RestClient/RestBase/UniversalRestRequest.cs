﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace YotaRest.Requests
{
    public class UniversalRestRequest
    {
        protected UriBuilder uri;
        protected HttpWebRequest http;
        protected HttpMethod method = HttpMethod.Get;
        protected HttpWebResponse response;
        protected string textResponse;
        protected string path;
        protected Dictionary<string, string> pathParams = new Dictionary<string, string>();
        protected Dictionary<string, string> queryParams = new Dictionary<string, string>();
        protected Dictionary<string, string> headerParams = new Dictionary<string, string>();
        private string strdata;

        public UniversalRestRequest(string path)
        {
            this.path = path;
        }

        public UniversalRestRequest(string basePath, string relativePath)
        {
            this.path = basePath + "/" + relativePath;
        }

        public string Response => textResponse;

        public HttpMethod Method { get; set; }
        public bool IsError { get; private set; }
        public Exception Error { get; private set; }

        public void PathParam(string key, string value)
        {
            if (pathParams == null) pathParams = new Dictionary<string, string>();
            pathParams.Add(key, value);
        }

        public void QueryParam(string key, string value)
        {
            if (queryParams == null) queryParams = new Dictionary<string, string>();
            queryParams.Add(key, value);
        }

        public void HeaderParam(string key, string value)
        {
            if (headerParams == null) headerParams = new Dictionary<string, string>();
            headerParams.Add(key, value);
        }

        public void Data(string value)
        {
            strdata = value;
        }

        public void Send()
        {
            http = CreateHttp();

            try
            {
                var _response = http.GetResponse();
                response = (HttpWebResponse)_response;
                using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
                {
                    textResponse = rdr.ReadToEnd();
                }
                IsError = false;
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ConnectFailure)
                {
                    Error = e;
                    IsError = true;
                    textResponse = JsonConvert.SerializeObject(new
                    {
                        Text = "Не удалось обратиться к точке удаленной службы на транспортном уровне.",
                        Status = e.Status,
                        Exception = e
                    });
                }
                else
                {
                    using (WebResponse response = e.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        using (Stream data = response.GetResponseStream())
                        using (var reader = new StreamReader(data))
                        {
                            textResponse = reader.ReadToEnd();
                            IsError = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex;
                IsError = true;
            }
        }

        private HttpWebRequest CreateHttp()
        {
            var _path = path;
            foreach (var item in pathParams)
            {
                _path = _path.Replace($"{{{item.Key}}}", item.Value);
            }
            uri = new UriBuilder(_path);

            StringBuilder _query = new StringBuilder();
            bool isFirst = true;
            foreach (var item in queryParams)
            {
                if (isFirst)
                    isFirst = false;
                else
                    _query.Append("&");

                _query.Append(item.Key);
                _query.Append("=");
                _query.Append(item.Value);
            }
            uri.Query = _query.ToString();

            HttpWebRequest http = WebRequest.CreateHttp(uri.Uri);
            http.Method = method.Method;
            foreach (var item in headerParams)
            {
                if (item.Key == "accept")
                    http.Accept = item.Value;
                else
                    http.Headers.Add(item.Key, item.Value);
            }

            return http;
        }
    }
}