﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace hty.Rest.Yota
{
    public abstract class ARestClient
    {
        private string basePath;

        protected string BasePath  => basePath; 

        public ARestClient(string basePath)
        {
            this.basePath = basePath;
        }

        public async Task<TResponse> GetAsync<TResponse>(string relativePath)
        {
            UriBuilder uri = new UriBuilder(basePath + "/" + relativePath);
            HttpWebRequest http = CreateHttp(uri.Uri, HttpMethod.Get);
            return await GetResponseAndJsonDeserializeAsync<TResponse>(http);
        }



        protected virtual void DefaultHeaders(HttpWebRequest http)
        {
            http.UserAgent = ".Net App";
        }

        private HttpWebRequest CreateHttp(Uri uri, HttpMethod method)
        {
            HttpWebRequest http = WebRequest.CreateHttp(uri.AbsoluteUri);
            http.Method = method.Method;
            DefaultHeaders(http);
            return http;
        }

        public string GetString(string relativePath)
        {
            UriBuilder uri = new UriBuilder(basePath + "/" + relativePath);
            HttpWebRequest http = CreateHttp(uri.Uri, HttpMethod.Get);
            HttpWebResponse response = (HttpWebResponse)http.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }
            return result;
        }

        public TResponse Get<TResponse>(string relativePath)
        {
            UriBuilder uri = new UriBuilder(basePath + "/" + relativePath);
            HttpWebRequest http = CreateHttp(uri.Uri, HttpMethod.Get);
            return GetResponseAndJsonDeserialize<TResponse>(http);
        }

        public string PostString(string relativePath, string data)
        {
            UriBuilder uri = new UriBuilder(basePath + "/" + relativePath);
            HttpWebRequest http = CreateHttp(uri.Uri, HttpMethod.Post);
            http.ContentType = "application/json";
            byte[] buf = Encoding.UTF8.GetBytes(data);
            http.ContentLength = buf.Length;
            var stream = http.GetRequestStream();
            stream.Write(buf, 0, buf.Length);
            HttpWebResponse response = (HttpWebResponse)http.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }
            return result;
        }

        private static async Task<TResponse> GetResponseAndJsonDeserializeAsync<TResponse>(HttpWebRequest http)
        {
            HttpWebResponse response = (HttpWebResponse)(await http.GetResponseAsync());
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }
            
            var responseObj = JsonConvert.DeserializeObject<TResponse>(result);
            //var responseObj = JsonSerializer.Deserialize<TResponse>(result);
            return responseObj;
        }

        private static TResponse GetResponseAndJsonDeserialize<TResponse>(HttpWebRequest http)
        {
            HttpWebResponse response = (HttpWebResponse)http.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }
            var responseObj = JsonConvert.DeserializeObject<TResponse>(result);
            //var responseObj = JsonSerializer.Deserialize<TResponse>(result);
            return responseObj;
        }
    }
}
