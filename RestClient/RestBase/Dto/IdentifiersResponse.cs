﻿using System;

namespace hty.Rest.Yota.Dto.Rids
{
    [Serializable]
    public class IdentifiersResponse
    {
        public string msisdn { get; set; }
        public string iccid { get; set; }
        public string imsi { get; set; }
        public string customerId { get; set; }
        public int accountId { get; set; }
        public int simExternalId { get; set; }
        public DateTime lastUpdated { get; set; }
    }
}