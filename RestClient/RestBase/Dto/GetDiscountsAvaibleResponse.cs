﻿using System;
using System.Collections.Generic;

namespace hty.Rest.Yota.Dto
{
    namespace Dms
    {
        [Serializable]
        public class GetDiscountsAvaibleResponse
        {
            public List<AnyDiscount> anyDiscounts { get; set; }
            public List<CurrentProduct> currentProducts { get; set; }
            public List<CurrentDiscount> currentDiscounts { get; set; }
        }

        
    }
}