﻿namespace hty.Rest.Yota
{
    public enum IdType
    {
        msisdn,
        iccid,
        imsi,
        customerId,
        accountId, 
        simExternalId
    }
}
