﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace hty.Rest.Yota.Dto
{
    [Serializable]
    public class LivingPeriod
    {
        public DateTime fromDate { get; set; }
    }
    [Serializable]
    public class PersonalDataAttribute
    {
        public string attributeUUId { get; set; }
        public string customerId { get; set; }
        public LivingPeriod livingPeriod { get; set; }
        public bool primary { get; set; }
        public string rawSource { get; set; }
    }
    [Serializable]
    public class Address
    {
        public PersonalDataAttribute personalDataAttribute { get; set; }
        public string addressText { get; set; }
        public string country { get; set; }
    }
    [Serializable]
    public class Document
    {
        public PersonalDataAttribute personalDataAttribute { get; set; }
        public string departmentCode { get; set; }
        public string documentNumber { get; set; }
        public string documentSeries { get; set; }
        public string documentType { get; set; }
        public DateTime expiryDate { get; set; }
        public string issueAuthority { get; set; }
        public DateTime issueDate { get; set; }
    }
    [Serializable]
    public class Name
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
    }
    [Serializable]
    public class PersonalDetails
    {
        public DateTime birthDate { get; set; }
        public string birthPlace { get; set; }
    }

    [Serializable]
    public class OfferDetails
    {
        public string name { get; set; }
        public string detailDescription { get; set; }
        public List<string> productTypes { get; set; }
        public long externalId { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
        public List<Price> prices { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
    [Serializable]
    public class AnyDiscount
    {
        public ApplyingAnyDiscount applyingAnyDiscount { get; set; }
        public OfferDetails offerDetails { get; set; }
    }
    [Serializable]
    public class ApplyingAnyDiscount
    {
        public string offerCode { get; set; }
        public int discountPct { get; set; }
        public DiscountPeriods discountPeriods { get; set; }
        public string discountOffer { get; set; }
        public DiscountEnds discountEnds { get; set; }
    }
    [Serializable]
    public class DiscountPeriods
    {
        public DateTime byDate { get; set; }
        public int byDays { get; set; }
        public int byProlongCount { get; set; }
    }
    [Serializable]
    public class DiscountEnds
    {
        public bool finblockProductEnd { get; set; }
        public bool changeProductEnd { get; set; }
        public bool changeResourceEnd { get; set; }
        public bool changeResourceBlock { get; set; }
    }
    [Serializable]
    public class CurrentProduct
    {
        public object id { get; set; }
        public int accountId { get; set; }
        public string offerCode { get; set; }
        public string name { get; set; }
        public string productSpecCode { get; set; }
        public List<string> productTypes { get; set; }
        public bool allowAutoProlong { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public Cost cost { get; set; }
        public DiscountCost discountCost { get; set; }
        public int externalId { get; set; }
        public string regionOfBuying { get; set; }
        public string status { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
        public string productLine { get; set; }
        public long? correlationProductId { get; set; }
        public List<Resource> resources { get; set; }
        public List<Option> options { get; set; }
        public List<Price> prices { get; set; }
        public string detailDescription { get; set; }
    }
    [Serializable]
    public class Option
    {
        public long id { get; set; }
        public string offerCode { get; set; }
        public int externalId { get; set; }
        public string name { get; set; }
        public string detailDescription { get; set; }
        public long masterProductId { get; set; }
        public string productSpecCode { get; set; }
        public bool allowAutoProlong { get; set; }
        public List<string> optionTypes { get; set; }
        public Cost cost { get; set; }
        public DiscountCost discountCost { get; set; }
        public List<long> discountIds { get; set; }
        public Resource resource { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
    }
    [Serializable]
    public class CurrentDiscount
    {
        public long id { get; set; }
        public string offerCode { get; set; }
        public int category { get; set; }
        public string reason { get; set; }
        public List<string> discountTypes { get; set; }
        public int externalId { get; set; }
        public string name { get; set; }
        public string detailDescription { get; set; }
        public List<long> relatedProductIds { get; set; }
        public Amount amount { get; set; }
        public List<string> applicationLevel { get; set; }
        public string status { get; set; }
        public int resourceDisconnect { get; set; }
        public bool productDisconnect { get; set; }
        public bool finBlockDisconnect { get; set; }
        public int maxProlongNumber { get; set; }
        public int remainingNumber { get; set; }
        public DateTime expirationDate { get; set; }
        public string productSpecCode { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
    }
    [Serializable]
    public class Status
    {
        public DateTime changed { get; set; }
        public string status { get; set; }
    }
    [Serializable]
    public class Cost
    {
        public double amount { get; set; }
        public string currencyCode { get; set; }
    }
    [Serializable]
    public class DiscountCost
    {
        public double amount { get; set; }
        public string currencyCode { get; set; }
    }
    [Serializable]
    public class Term
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("unitOfMeasure")]
        public string UnitOfMeasure { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
    [Serializable]
    public class Characteristic
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("unitOfMeasure")]
        public string UnitOfMeasure { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
    [Serializable]
    public class TermsAndCharacteristics
    {
        [JsonProperty("characteristics")]
        public List<Characteristic> Characteristics { get; set; }

        [JsonProperty("terms")]
        public List<Term> Terms { get; set; }
    }
    [Serializable]
    public class Price
    {
        public string code { get; set; }
        public string name { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public string priceCalcCode { get; set; }
        public string productSpecCode { get; set; }
        public string tarifficationType { get; set; }
        public string unitOfMeasure { get; set; }
        public bool isDefault { get; set; }
        public List<string> availabilityByCriteriaRuleCode { get; set; }
        public double amount { get; set; }
        public string currencyCode { get; set; }
        public PriceItem price { get; set; }
        public string descriptionUrl { get; set; }
        public string description { get; set; }
        public List<string> priceTypes { get; set; }
    }
    [Serializable]
    public class CharValueUse
    {
        public string specCharCode { get; set; }
        public string unitOfMeasure { get; set; }
        public string value { get; set; }
    }
    [Serializable]
    public class CalculationRule
    {
        public string code { get; set; }
        public string name { get; set; }
        public string specCode { get; set; }
        public List<CharValueUse> charValueUse { get; set; }
        public string dependency { get; set; }
    }
    [Serializable]
    public class PriceItem
    {
        public Price price { get; set; }
        public CalculationRule calculationRule { get; set; }
    }
    [Serializable]

    public class Resource
    {
        [JsonProperty("offerCode")]
        public string OfferCode { get; set; }

        [JsonProperty("resourceType")]
        public string ResourceType { get; set; }

        [JsonProperty("productSpecCode")]
        public string ProductSpecCode { get; set; }

        [JsonProperty("capacity")]
        public string Capacity { get; set; }

        [JsonProperty("unitOfMeasure")]
        public string UnitOfMeasure { get; set; }

        [JsonProperty("priceCode")]
        public string PriceCode { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("detailDescription")]
        public string DetailDescription { get; set; }

        [JsonProperty("prices")]
        public List<Price> Prices { get; set; }

        [JsonProperty("termsAndCharacteristics")]
        public TermsAndCharacteristics TermsAndCharacteristics { get; set; }

        [JsonProperty("discountIds")]
        public List<int> DiscountIds { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("externalId")]
        public int ExternalId { get; set; }

        [JsonProperty("resourceTypes")]
        public List<string> ResourceTypes { get; set; }

        [JsonProperty("cost")]
        public Cost Cost { get; set; }

        [JsonProperty("discountCost")]
        public DiscountCost DiscountCost { get; set; }

        [JsonProperty("accumLeft")]
        public int AccumLeft { get; set; }

        [JsonProperty("accumId")]
        public string AccumId { get; set; }
    }



    [Serializable]
    public class AvailableProductOption
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("offerCode")]
        public string OfferCode { get; set; }

        [JsonProperty("masterProductId")]
        public int MasterProductId { get; set; }

        [JsonProperty("isAutoprolongEnabled")]
        public bool IsAutoprolongEnabled { get; set; }

        [JsonProperty("resource")]
        public Resource Resource { get; set; }

        [JsonProperty("additionalProperties")]
        public AdditionalProperties AdditionalProperties { get; set; }

        [JsonProperty("prolongationCount")]
        public int ProlongationCount { get; set; }

        [JsonProperty("expiryDate")]
        public DateTime ExpiryDate { get; set; }

        [JsonProperty("discountId")]
        public int DiscountId { get; set; }

        [JsonProperty("applyingOption")]
        public ApplyingOption ApplyingOption { get; set; }

        [JsonProperty("offerDetails")]
        public OfferDetails OfferDetails { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("externalId")]
        public int ExternalId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("detailDescription")]
        public string DetailDescription { get; set; }

        [JsonProperty("productSpecCode")]
        public string ProductSpecCode { get; set; }

        [JsonProperty("allowAutoProlong")]
        public bool AllowAutoProlong { get; set; }

        [JsonProperty("optionTypes")]
        public List<string> OptionTypes { get; set; }

        [JsonProperty("cost")]
        public Cost Cost { get; set; }

        [JsonProperty("discountCost")]
        public DiscountCost DiscountCost { get; set; }

        [JsonProperty("productIdToChange")]
        public int ProductIdToChange { get; set; }

        [JsonProperty("discountIds")]
        public List<int> DiscountIds { get; set; }

        [JsonProperty("termsAndCharacteristics")]
        public TermsAndCharacteristics TermsAndCharacteristics { get; set; }
    }

    [Serializable]
    public class ApplyingOption
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("offerCode")]
        public string OfferCode { get; set; }

        [JsonProperty("masterProductId")]
        public int MasterProductId { get; set; }

        [JsonProperty("isAutoprolongEnabled")]
        public bool IsAutoprolongEnabled { get; set; }

        [JsonProperty("resource")]
        public Resource Resource { get; set; }

        [JsonProperty("additionalProperties")]
        public AdditionalProperties AdditionalProperties { get; set; }

        [JsonProperty("prolongationCount")]
        public int ProlongationCount { get; set; }

        [JsonProperty("expiryDate")]
        public DateTime ExpiryDate { get; set; }

        [JsonProperty("discountId")]
        public int DiscountId { get; set; }
    }

    [Serializable]
    public class AvailableProduct
    {
        [JsonProperty("applyingProduct")]
        public ApplyingProduct ApplyingProduct { get; set; }

        [JsonProperty("offerDetails")]
        public OfferDetails OfferDetails { get; set; }

        [JsonProperty("options")]
        public List<Option> Options { get; set; }
    }

    [Serializable]
    public class ApplyingProduct
    {
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("offerCode")]
        public string OfferCode { get; set; }

        [JsonProperty("maOfferCode")]
        public string MaOfferCode { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("isAutoprolongEnabled")]
        public bool IsAutoprolongEnabled { get; set; }

        [JsonProperty("resources")]
        public List<Resource> Resources { get; set; }

        [JsonProperty("options")]
        public List<AvailableProductOption> Options { get; set; }

        [JsonProperty("priceCode")]
        public string PriceCode { get; set; }

        [JsonProperty("maPriceCode")]
        public string MaPriceCode { get; set; }

        [JsonProperty("price")]
        public int Price { get; set; }

        [JsonProperty("totalAmount")]
        public int TotalAmount { get; set; }

        [JsonProperty("additionalProperties")]
        public AdditionalProperties AdditionalProperties { get; set; }
    }
    
    [Serializable]
    public class AdditionalProperties
    {
        [JsonProperty("additionalProp1")]
        public string AdditionalProp1 { get; set; }

        [JsonProperty("additionalProp2")]
        public string AdditionalProp2 { get; set; }

        [JsonProperty("additionalProp3")]
        public string AdditionalProp3 { get; set; }
    }

    [Serializable]
    public class Product
    {
        public object id { get; set; }
        public int accountId { get; set; }
        public string offerCode { get; set; }
        public string name { get; set; }
        public string productSpecCode { get; set; }
        public List<string> productTypes { get; set; }
        public bool allowAutoProlong { get; set; }
        public DateTime beginDate { get; set; }
        public DateTime endDate { get; set; }
        public Cost cost { get; set; }
        public DiscountCost discountCost { get; set; }
        public int externalId { get; set; }
        public string regionOfBuying { get; set; }
        public string status { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
        public string productLine { get; set; }
        public long? correlationProductId { get; set; }
        public List<Resource> resources { get; set; }
        public List<PriceItem> prices { get; set; }
        public string detailDescription { get; set; }
    }
    [Serializable]
    public class Amount
    {
        public int amount { get; set; }
        public string unitOfMeasure { get; set; }
    }
    [Serializable]
    public class Discount
    {
        public string id { get; set; }
        public string offerCode { get; set; }
        public int category { get; set; }
        public string reason { get; set; }
        public List<string> discountTypes { get; set; }
        public int externalId { get; set; }
        public string name { get; set; }
        public string detailDescription { get; set; }
        public Amount amount { get; set; }
        public List<string> applicationLevel { get; set; }
        public string status { get; set; }
        public int resourceDisconnect { get; set; }
        public bool productDisconnect { get; set; }
        public bool finBlockDisconnect { get; set; }
        public int maxProlongNumber { get; set; }
        public int remainingNumber { get; set; }
        public DateTime expirationDate { get; set; }
        public string productSpecCode { get; set; }
        public TermsAndCharacteristics termsAndCharacteristics { get; set; }
    }
    [Serializable]
    public class Email
    {
        public bool primary { get; set; }
        public string type { get; set; }
        public string email { get; set; }
    }
    [Serializable]
    public class MobileApplication
    {
        public string customerId { get; set; }
        public bool primary { get; set; }
        public string appId { get; set; }
        public string appVersion { get; set; }
        public string os { get; set; }
        public string osVersion { get; set; }
        public string model { get; set; }
        public string imei { get; set; }
        public string pushToken { get; set; }
        public bool receivesSMS { get; set; }
    }
    [Serializable]
    public class Phone
    {
        public bool primary { get; set; }
        public string phoneNumber { get; set; }
        public string type { get; set; }
        public string timezone { get; set; }
    }
    [Serializable]
    public class CisCustomer
    {
        public string customerId { get; set; }
        public string status { get; set; }
        public string customerPurpose { get; set; }
        public string marketingSegment { get; set; }
        public string type { get; set; }
        public List<Email> emails { get; set; }
        public List<Phone> phones { get; set; }
        public List<MobileApplication> mobileApplications { get; set; }
    }
}