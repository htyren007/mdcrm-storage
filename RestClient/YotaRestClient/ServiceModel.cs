﻿using WPFStorage.Base;

namespace YotaRestClient
{
    internal class MethodModel : ObservableObject
    { 
    
    }
    internal class ServiceModel : ObservableObject 
    {
        private string title;

        public string Title { get => title; set => SetProperty(ref title, value); }

        private MethodModel method;

        public MethodModel Method { get => method; set => SetProperty(ref method, value); }
    }
}