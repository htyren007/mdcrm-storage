﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFStorage.Base;

namespace YotaRestClient.PassWin
{
    /// <summary>
    /// Логика взаимодействия для PassWin.xaml
    /// </summary>
    public partial class PassWin : Window
    {
        public PassWin()
        {
            InitializeComponent();
            DataContext = new PassWinModel();
        }
    }

    internal class PassWinModel : ObservableObject
    {
        private string login = "yotaru\\rmiroshnikov";
        private string pass = "pGDDuYhE!1Vj";
        private string admlogin = "scarteldc\\yt-crm-adm";
        private string admpass = "C629952.<SG8752";
        private string devlogin = "YOTA\\rmiroshnikov";
        private string devpass = "12Qwerty";

        public string Login { get => login; set => SetProperty(ref login, value); }
        public string Password { get => pass; set => SetProperty(ref pass, value); }

        public string AdmLogin { get => admlogin; set => SetProperty(ref admlogin, value); }
        public string AdmPassword { get => admpass; set => SetProperty(ref admpass, value); }

        public string DevLogin { get => devlogin; set => SetProperty(ref devlogin, value); }
        public string DevPassword { get => devpass; set => SetProperty(ref devpass, value); }

        public PassWinModel()
        {
            CopyLoginCommand = new RelayCommand(()=> Clipboard.SetText(login));
            CopyPassCommand = new RelayCommand(() => Clipboard.SetText(pass));
            CopyAdmLoginCommand = new RelayCommand(() => Clipboard.SetText(admlogin));
            CopyAdmPassCommand = new RelayCommand(() => Clipboard.SetText(admpass));
            CopyDevLoginCommand = new RelayCommand(() => Clipboard.SetText(devlogin));
            CopyDevPassCommand = new RelayCommand(() => Clipboard.SetText(devpass));
        }
        public RelayCommand CopyLoginCommand { get; }
        public RelayCommand CopyPassCommand { get; }
        public RelayCommand CopyAdmLoginCommand { get; }
        public RelayCommand CopyAdmPassCommand { get; }
        public RelayCommand CopyDevLoginCommand { get; }
        public RelayCommand CopyDevPassCommand { get; }
    }
}
