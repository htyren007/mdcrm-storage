﻿using System;
using WPFStorage.Base;

namespace YotaRestClient
{
    internal class MainWindowModel : ObservableObject
    {
        private ServiceModel service = new ServiceModel();

        public ServiceModel Service { get => service; set => SetProperty(ref service, value); }
        public RelayCommand OpenPassWin { get; }
        public RelayCommand SetDmsServiceCommand { get; }

        public MainWindowModel()
        {
            OpenPassWin = new RelayCommand(OpenPassWinMethod);
            SetDmsServiceCommand = new RelayCommand(SetDmsService);
        }

        private void SetDmsService()
        {
            throw new NotImplementedException();
        }

        private void OpenPassWinMethod()
        {
            var win = new PassWin.PassWin();
            win.Show();
        }
    }
}